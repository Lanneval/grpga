/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class BaseItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareDerivedData() {
    const itemData = this.data.data;
    switch (this.type) {
      case "Variable": {
        let entries = Object.values(itemData.entries);
        for (let i = 1; i < entries.length; i++) {
          entries[i].value = Number(entries[i].formula) || 0; // recalculate the value
          // if the entry label matches the variable label, write the formula and value
          if (entries[i].label == itemData.label) {
            itemData.moddedformula = itemData.formula = entries[i].formula;
            itemData.value = entries[i].value;
          }
        }
        break;
      }
      case "Modifier": {
        itemData.attack = false;
        itemData.damage = false;
        itemData.defence = false;
        itemData.reaction = false;
        itemData.skill = false;
        itemData.spell = false;
        itemData.check = false;
        itemData.primary = false;
        let entries = Object.values(itemData.entries);
        for (let i = 1; i < entries.length; i++) { //ignore category changes to [0]
          switch (entries[i].category) {
            case "attack": itemData.attack = true; break;
            case "damage": itemData.damage = true; break;
            case "defence": itemData.defence = true; break;
            case "reaction": itemData.reaction = true; break;
            case "skill": itemData.skill = true; break;
            case "spell": itemData.spell = true; break;
            case "check": itemData.check = true; break;
            case "primary": itemData.primary = true; break;
          }
        }
        break;
      }
      case "Melee-Attack":
      case "Ranged-Attack":
      case "Defence":
      case "Rollable": {
        if (Number.isNumeric(itemData.formula)) {
          // the formula is a number
          itemData.value = Number(itemData.formula);
        } else if (itemData.formula.includes("#") || itemData.formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          itemData.value = 0;
          itemData.moddedvalue = 0;
          itemData.moddedformula = itemData.formula;
        }
        break;
      }
      default: {
        // do nothing yet
      }
    }
  }
}
