/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class BaseItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "item"],
      width: 450,
      height: 450,
    });
  }

  /** @override */
  get template() {
    const path = "systems/grpga/templates/item";
    // Return a single sheet for all item types.
    // return `${path}/item-sheet.hbs`;
    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.hbs`.

    return `${path}/${this.item.type.toLowerCase()}-sheet.hbs`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    // Retrieve base data structure.
    const data = super.getData(options);

    // Grab the item's data.
    const itemData = data.data;

    // Re-define the template data references.
    data.item = itemData;
    data.data = itemData.data;

    data.config = CONFIG.grpga;
    data.mode = this.actor?.data.data.mode || data.config.ruleset;
    data.modeSpecificText = {
      mode: data.mode,
      check: game.i18n.localize(`grpga.item.rollable.${data.mode}.check`),
      skill: game.i18n.localize(`grpga.item.rollable.${data.mode}.skill`),
      spell: game.i18n.localize(`grpga.item.rollable.${data.mode}.spell`),
      technique: game.i18n.localize(`grpga.item.rollable.${data.mode}.technique`),
      rms: game.i18n.localize(`grpga.item.rollable.${data.mode}.rms`),
      advantage: game.i18n.localize(`grpga.item.trait.${data.mode}.advantage`),
      disadvantage: game.i18n.localize(`grpga.item.trait.${data.mode}.disadvantage`),
      perk: game.i18n.localize(`grpga.item.trait.${data.mode}.perk`),
      quirk: game.i18n.localize(`grpga.item.trait.${data.mode}.quirk`),
      accuracy: game.i18n.localize(`grpga.item.ranged-attack.${data.mode}.accuracy`),
      attr: game.i18n.localize(`grpga.item.primary-attribute.${data.mode}.attr`),
      defence: game.i18n.localize(`grpga.item.modifier.${data.mode}.defence`),
      reaction: game.i18n.localize(`grpga.item.modifier.${data.mode}.reaction`),
      targets: game.i18n.localize(`grpga.item.modifier.${data.mode}.targets`),
      formula: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.formula`),
      armourDiv: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.armourDiv`),
      minST: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.minST`),
      damage: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.damage`),
      damageType: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.damageType`),
      dodge: game.i18n.localize(`grpga.item.defence.${data.mode}.dodge`),
      block: game.i18n.localize(`grpga.item.defence.${data.mode}.block`),
      parry: game.i18n.localize(`grpga.item.defence.${data.mode}.parry`)
    }
    return data;
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = /@([\w.\-]+)/gi;
    let findTerms = (match, term) => {
      let value = this.actor?.data.data.dynamic[term]?.value || this.actor?.data.data.tracked[term]?.value;
      return (value) ? String(value).trim() : "0";
    };
    return { value: formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)/g, "Math.$&") };
  }
}
export class PoolItemSheet extends BaseItemSheet {

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.pool').change(this._onInputChange.bind(this));
  }

  /**
   * Handle the Item Input Change Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onInputChange(event) { // only for pools as they must update tokens
    if (CONFIG.grpga.testMode) console.debug("entering _onInputChange()\n", event);

    const target = event.currentTarget;
    // get the name of the changed element
    const dataname = target.name.split(".")[1];
    // get the new value
    let value = target.value;
    // is this the value attribute, isBar is true
    let isBar = (dataname == "value");

    this.actor.modifyTokenAttribute(
      `tracked.${this.item.data.data.abbr.toLowerCase()}${isBar ? '' : `.${dataname}`}`,
      value,
      false, isBar
    );
  }
}

export class ModifierItemSheet extends BaseItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "item", "modifier"],
      width: 600,
      height: 400,
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.modentry-create').click(this._onModEntryCreate.bind(this));
    html.find('.modentry-delete').click(this._onModEntryDelete.bind(this));
    html.find('.modentry-clone').click(this._onModEntryClone.bind(this));
    html.find('.modentry-moveup').click(this._onModEntryMoveUp.bind(this));
  }

  /**
   * Handle the Modifier Entry Create click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryCreate(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onModEntryCreate()\n", event);
    let itemdata = this.item.data.data;

    // no formula, must be accident
    if (itemdata.entries[0].formula.trim() == "") return;

    let modentries = Object.values(itemdata.entries);
    let entry = modentries[0];
    let formData = this._replaceData(entry.formula);
    try {
      entry.value = Math.round(eval(formData.value.replace(CONFIG.grpga.dataRgx, '')));
      entry.moddedformula = "" + entry.value; // store the result as a String
    } catch (err) {
      // store the formula ready to be rolled
      entry.moddedformula = formData.value;
      entry.value = 0; // eliminate any old values
      console.debug("Modifier formula evaluation error:\n", [entry.moddedformula]);
    }
    modentries.push(entry);
    modentries[0] = { value: 0, formula: "", category: "", targets: "" };
    let data = {
      chartype: itemdata.chartype,
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Modifier Entry Delete click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryDelete(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onModEntryDelete()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".modentry").dataset.id;

    let itemdata = this.item.data.data;
    delete itemdata.entries[index];
    let modentries = Object.values(itemdata.entries);

    let data = {
      chartype: itemdata.chartype,
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Modifier Entry Clone click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryClone(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onModEntryClone()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".modentry").dataset.id;

    let itemdata = this.item.data.data;
    let modentries = Object.values(itemdata.entries);
    modentries.push({ ...modentries[index] });

    let data = {
      chartype: itemdata.chartype,
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Modifier Entry Clone click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryMoveUp(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onModEntryMoveUp()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".modentry").dataset.id;

    let itemdata = this.item.data.data;
    let modentries = Object.values(itemdata.entries);
    [modentries[index - 1], modentries[index]] = [modentries[index], modentries[index - 1]];

    let data = {
      chartype: itemdata.chartype,
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'data': data });
  }
}

export class VariableItemSheet extends BaseItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "item", "variable"],
      width: 300,
      height: 400,
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.varentry-create').click(this._onVarEntryCreate.bind(this));
    html.find('.varentry-delete').click(this._onVarEntryDelete.bind(this));
    html.find('.varentry-clone').click(this._onVarEntryClone.bind(this));
    html.find('.varentry-moveup').click(this._onVarEntryMoveUp.bind(this));
  }

  /**
   * Handle the Variable Entry Create click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryCreate(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onVarEntryCreate()\n", event);
    let itemdata = this.item.data.data;

    // no formula, must be accident
    if (itemdata.entries[0].formula.trim() == "") return;

    let varentries = Object.values(itemdata.entries);
    let entry = varentries[0];
    let formData = this._replaceData(entry.formula);
    try {
      entry.value = Math.round(eval(formData.value.replace(CONFIG.grpga.dataRgx, '')));
      entry.moddedformula = "" + entry.value; // store the result as a String
    } catch (err) {
      // store the formula ready to be rolled
      entry.moddedformula = formData.value;
      entry.value = 0; // eliminate any old values
      console.debug("Modifier formula evaluation error:\n", [entry.moddedformula]);
    }
    varentries.push(entry);
    varentries[0] = { value: 0, formula: "", label: "" };
    let data = {
      chartype: itemdata.chartype,
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Variable Entry Delete click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryDelete(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onVarEntryDelete()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".varentry").dataset.id;

    let itemdata = this.item.data.data;
    delete itemdata.entries[index]; // not an array at this point
    let varentries = Object.values(itemdata.entries);

    let data = {
      chartype: itemdata.chartype,
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Variable Entry Clone click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryClone(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onVarEntryClone()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".varentry").dataset.id;

    let itemdata = this.item.data.data;
    let varentries = Object.values(itemdata.entries);
    varentries.push({ ...varentries[index] });

    let data = {
      chartype: itemdata.chartype,
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Variable Entry MoveUp click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryMoveUp(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onVarEntryMoveUp()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".varentry").dataset.id;

    let itemdata = this.item.data.data;
    let varentries = Object.values(itemdata.entries);
    [varentries[index - 1], varentries[index]] = [varentries[index], varentries[index - 1]];

    let data = {
      chartype: itemdata.chartype,
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'data': data });
  }
}
