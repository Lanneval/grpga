import { grpga } from "../config.js";

export class GRPGACombat extends Combat {

  async nextRound() {
    this.combatants.forEach(c => c.actor.resetTemporaryItems());
    return super.nextRound();
  }
}
