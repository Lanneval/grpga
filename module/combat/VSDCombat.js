import { grpga } from "../config.js";

export class VSDCombat extends Combat {
  constructor(data, context) {
    super(data, context);
    this.actions = grpga.vsd.combat || [];
  }

  /**
   * @override
   */
  async rollAll(options) {
    await super.rollAll(options);
    // make sure the top combatant is selected
    return this.update({ turn: 0 });
  }

  async nextTurn() {
    await this.combatant.update({ ["flags.grpga.action"]: this.actions[0].action,  initiative: null });
    await this.combatant.actor.setFlag("grpga", "action", "Completed");

    return this.update({ turn: 0 });
  }

  async nextRound() {
    this.combatants.forEach(c => c.actor.resetTemporaryItems());
    return super.nextRound();
  }
}

export class VSDCombatTracker extends CombatTracker {
  get template() {
    return "systems/grpga/templates/combat/vsdcombat-tracker.hbs";
  }

  async getData(options) {
    const data = await super.getData(options);
    if (!data.hasCombat) {
      return data;
    }
    for (let turn of data.turns) {
      let combatant = this.viewed.combatants.get(turn.id);
      turn.action = combatant.getFlag("grpga", "action");
    }
    return data;
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find(".declaredaction").change(this._onActionsChanged.bind(this));
    html.find(".interrupt").click(this._onInterrupt.bind(this));
  }

  async _onActionsChanged(event) {
    const option = event.currentTarget.selectedOptions[0];
    const li = event.currentTarget.closest(".combatant");
    const combat = this.viewed;
    const c = combat.combatants.get(li.dataset.combatantId);

    await c.update({ ["flags.grpga.action"]: option.text, initiative: Number(option.value) });
    await c.actor.setFlag("grpga", "action", option.text);
    this.render();
  }

  async _onInterrupt(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const combat = this.viewed;
    const c = combat.combatants.get(li.dataset.combatantId);
    const currentTurn = combat.turns[0];

    await c.update({ initiative: currentTurn.initiative + 1 });
    this.render();
  }

}

export class VSDCombatantConfig extends CombatantConfig {
  get template() {
    return "systems/grpga/templates/combat/vsdcombatant-config.hbs";
  }
}

export class VSDCombatant extends Combatant {
  constructor(data, context) {
    super(data, context);
  }

  _onCreate(data, options, userId) {
    super._onCreate(data, options, userId);
    this.setFlag("grpga", "action", "Select");
    data.initiative = null;
  }
}
