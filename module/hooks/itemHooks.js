Hooks.on('createItem', (document, options, userId) => {
  if (CONFIG.grpga.testMode) console.debug("createItem:\n", [document, options, userId]);

  // items dragged from the sidebar will lose their moddedformula if they have one so calculate it again
  switch (document.type) {
    case "Melee-Attack":
    case "Ranged-Attack":
    case "Defence":
    case "Rollable": {
      let docdata = document.data.data;
      if (CONFIG.grpga.testMode) console.debug("Item Data:\n", docdata);
      if (docdata.formula) { // we will recalculate the value if the formula does not contain an @dependency
        let formula = docdata.formula;
        if (Number.isNumeric(formula)) {
          // the formula is a number
          docdata.value = Number(formula);
        } else if (formula.includes("#") || formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          docdata.value = 0;
          docdata.moddedvalue = 0;
          docdata.moddedformula = formula;
        }
      }
      break;
    }
  }
});

Hooks.on('preCreateItem', (document, data, options, userId) => {
  if (CONFIG.grpga.testMode) console.debug("preCreateItem:\n", [document, data, options, userId]);

  if (document.data._id) {// This item exists already but may have the wrong chartype
    if (document.actor) {
      // if there is an actor and the chartype matches then exit
      if (document.actor.type == data.data.chartype) return;
      data.data.chartype = document.actor.type;
    } else {
      // if the chartype matches the ruleset then exit
      if (CONFIG.grpga.chartype == data.data.chartype) return;
      data.data.chartype = CONFIG.grpga.chartype;
    }
    // correct the chartype then exit
    document.data.update(data);
    return;
  } else if (data.data == undefined) {
    // the item is being created from the item sidebar so give it a chartype
    data.data = document.data.data;
    data.data.chartype = CONFIG.grpga.chartype;
  }

  if (!data.data.group) { // initialise group field to match item category or type
    data.data.group = data.data.category || data.type;
  }

  switch (data.data.chartype) {
    case "CharacterD100": {
      switch (data.type) {
        case "Melee-Attack": {
          data.img = "icons/svg/sword.svg";
          data.data.armourDiv = 5;
          data.data.damage = "";
          data.data.damageType = "";
          data.data.minST = "oeh";
          data.data.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          data.data.reach = 5;
          data.data.weight = 100;
          break;
        }
        case "Ranged-Attack": {
          data.img = "icons/svg/target.svg";
          data.data.armourDiv = 5;
          data.data.damage = "";
          data.data.damageType = "";
          data.data.minST = "oeh";
          data.data.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          data.data.rof = 5;
          data.data.accuracy = 100;
          data.data.range = "10";
          break;
        }
        case "Hit-Location": { // Wound
          data.img = "icons/svg/blood.svg";
          data.name = "Wound";
          break;
        }
        case "Defence": { // dodge(std), parry(oeh), block(oe)
          data.img = "icons/svg/mage-shield.svg";
          data.name = "Various";
          break;
        }
        default: {
          data.img = "icons/svg/mystery-man-black.svg";
          break;
        }
      }
      break;
    }
    case "Character3D6": {
      switch (data.type) {
        case "Melee-Attack": {
          data.img = "icons/svg/sword.svg";
          data.data.formula = "@dx -2";
          data.data.armourDiv = 1;
          data.data.damage = "1d6";
          data.data.damageType = "cut";
          data.data.minST = "8";
          data.data.notes = ""
          data.data.reach = 1;
          data.data.weight = 0;
          break;
        }
        case "Ranged-Attack": {
          data.img = "icons/svg/target.svg";
          data.data.formula = "@dx -2";
          data.data.armourDiv = 1;
          data.data.damage = "1d6";
          data.data.damageType = "imp";
          data.data.minST = "8";
          data.data.notes = ""
          data.data.range = "10/15";
          data.data.rof = 1;
          data.data.accuracy = 0;
          data.data.shots = "0";
          data.data.bulk = 0;
          data.data.recoil = "0";
          break;
        }
        default: {
          data.img = "icons/svg/mystery-man-black.svg";
          break;
        }
      }
      break;
    }
    default: {
      if (!data.img) {
        data.img = "icons/svg/mystery-man-black.svg";
      }
      break;
    }
  }
  document.data.update(data);
});
