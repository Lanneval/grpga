import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorRtWSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorRtW-sheet.hbs",
      width: 650,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "main" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.grpga.testMode) console.debug("entering getData() in ActorRtW-sheet");

    let data = super.getData(options);
    let actor = data.actor;
    let actordata = actor.data;

    data.headerinfo = {};
    let headinfo = data.headerinfo;

    headinfo.level = actordata.dynamic.level;
    headinfo.perception = actordata.dynamic.perception;
    headinfo.initiative = actordata.dynamic.initiative;
    headinfo.dodge = actordata.dynamic.dodge;
    headinfo.xp = actordata.dynamic.xp;

    // prepare the traits for display
    data.power = []; data.wisdom = []; data.courage = [];
    for (let trait of Object.values(actordata.traits)) {
      switch (trait.aspect) {
        case "power":data.power.push(trait);break;
        case "wisdom":data.wisdom.push(trait);break;
        case "courage":data.courage.push(trait);break;
      }
    }

    return data;
  }

  sort(data) {
    return data.sort((a, b) => {
      if (a.sort < b.sort) return -1;
      if (a.sort > b.sort) return 1;
      return 0;
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importRtW').click(this._onImportRtWData.bind(this));
  }

  async _onImportRtWData(event) {
    event.preventDefault();
    let temp = this.actor.data.data.itemscript;
    if (temp[0] == "\"") { // remove the wrapping doublequotes
      temp = temp.substr(1, temp.length - 2);
    }
    const scriptdata = temp.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.data.data.biography;

    let ammoIndex = 0;
    for (let entry of scriptdata) {
      let regex = new RegExp(/([^:]+):([^\r|\n]+)/gi);
      const line = regex.exec(entry);

      switch (line[1].trim()) {
        case "Name": {
          // Name: {{name}}
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
        case "Attribute": {
          // Attribute: {{name}} ### {{abbr || slugify(name)}} ### {{attr || 0}} ### {{sort || 0}} ### {{notes || empty}}
          // Attribute: Intelligence Quotient ### IQ ### 12 ### 1 ### Your ability to reason
          let data = {
            type: "Primary-Attribute",
            data: {
              chartype: "CharacterD120",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1] || this.actor.slugify(result[0]);
          data.data.attr = Number(result[2]) || 0;
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Skill": {
          // Skill: {{name}} ### {{"VarForm" || "D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          // Skill: Athletics ### D100 ### 45 ### 3 ### Training in vigorous exertion for competition
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          if (!result[1]) {
            data.data.category = "spell";
          } else {
            switch (result[1].toLowerCase()) {
              case "varform":
                data.data.category = "check";
                break;
              case "d20":
                data.data.category = "skill";
                break;
              default:
                data.data.category = "spell";
            }
          }
          data.data.formula = result[2] || "0";
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Information": {
          // Information: {{name}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "You should be putting html formatted content here to be displayed in the chat log.";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Reference": {
          // Reference: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120",
              category: "rms"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Defence": {
          // Defence: {{name}} ### {{"D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          if (!result[1]) {
            data.data.category = "parry";
          } else {
            switch (result[1].toLowerCase()) {
              case "d20":
                data.data.category = "dodge";
                break;
              default:
                data.data.category = "parry";
            }
          }
          data.data.formula = result[2] || "0";
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Trait": {
          // Trait: {{name}} ### {{category}} ### {{notes}} ### {{sort || 0}}
          let data = {
            type: "Trait",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.category = result[1].toLowerCase();
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "SharedValue": {
          // SharedValue: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterD120",
              category: "block"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Pool": {
          // Pool: {{name}} ### {{abbr}} ### {{max}} ### {{value}} ### {{min}} ### {{sort || 50}} ### {{notes || empty}}
          let data = {
            type: "Pool",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1];
          data.data.max = result[2];
          data.data.value = result[3];
          data.data.min = result[4];
          data.data.sort = Number(result[5]) || 50;
          data.data.notes = result[6] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Attack": {
          // Attack: {{name}} ### {{sort || 50}} ### {{range}} ### {{damage}} ### {{rof}} ### {{payload}} ### {{ammo}} ### {{damage class}}
          let data = {
            data: {
              chartype: "CharacterD120",
              armourDiv: 20,
              minST: "x2"
            }
          }
          let result = line[2].split("###").map(word => word.trim());
          data.data.sort = Number(result[1]) || 50;
          data.data.damage = result[3];
          data.data.damageType = result[7];

          if (result[2] == "0") { // melee
            data.type = "Melee-Attack";
            data.name = `Melee: ${result[0]}`;
            data.data.formula = "@strike-bonus";
          } else { // ranged
            data.type = "Ranged-Attack";
            data.name = `Ranged: ${result[0]}`;
            data.data.formula = "0";
            data.data.accuracy = result[4];
            data.data.range = result[2];
            if (result[5] != "0") { // ammunition pool required
              let ammodata = {
                name: `${result[0]} Ammunition`,
                type: "Pool",
                data: {
                  abbr: `Ammo${++ammoIndex}`,
                  chartype: "CharacterD120",
                  min: 0,
                  value: Number(result[5]),
                  max: Number(result[5]),
                  notes: `Reloads: ${Number(result[6])}`
                }
              }
              const item = this.actor.data.items.find(i => i.name === ammodata.name);
              if (item) {
                // do not override the existing saved ammunition tracking
              } else {
                await this.actor.createEmbeddedDocuments('Item', [ammodata]);
              }
            }
          }
          const item = this.actor.data.items.find(i => i.name === data.name);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Armor Type": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name === "Armor Type");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Level": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Level-Ranks"));
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Race": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Race"));
          const update = { _id: item.id, 'name': `Race: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "CharacterClass": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Culture"));
          const update = { _id: item.id, 'name': `Culture: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Occupation": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Profession"));
          const update = { _id: item.id, 'name': `Profession: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Psionics": {
          // Psionics: {{name}} ### {{sort || 50}} ### {{ispcost}} ### {{range}} ### {{duration}} ### {{effects}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = "0";
          data.data.sort = Number(result[1]) || 50;
          data.data.notes = `<hr><div><b>ISP Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Effects:</b> ${result[5]}</div>`;
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Magic": {
          // Magic: {{name}} ### {{sort || 50}} ### {{ppecost}} ### {{range}} ### {{duration}} ### {{saves}} ### {{effects}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = "0";
          data.data.sort = Number(result[1]) || 50;
          data.data.notes = `<hr><div><b>PPE Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Saves:</b> ${result[5]}</div><div><b>Effects:</b> ${result[6]}</div>`;
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Melee": {
          // Melee: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{parry}}  ### {{dodge}}  ### {{autododge}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{pullpunch}}  ### {{rollwithpunch}}  ### {{notes  || empty}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[M]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          const strike = result[4];
          const parry = result[5];
          const dodge = result[6];
          const adodge = result[7];
          const disarm = result[8];
          const entangle = result[9];
          const damage = result[10] || "0";
          const pullpunch = result[11];
          const rollpunch = result[12];
          const notes = `${result[13]}\nLevel: ${result[14]}`;

          let nextEntry = 1;
          let data = {
            name: name,
            type: "Modifier",
            data: {
              chartype: "CharacterD120",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };

          if (initiative != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            data.data.defence = true;
          }
          if (actions != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            data.data.primary = true;
          }
          if (strike != 0) {
            data.data.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Melee"
            }
            data.data.attack = true;
          }
          if (parry != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(parry),
              formula: parry,
              category: "defence",
              targets: "Parry"
            }
            data.data.defence = true;
          }
          if (dodge != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(dodge),
              formula: dodge,
              category: "defence",
              targets: "Dodge"
            }
            data.data.defence = true;
          }
          if (adodge != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(adodge),
              formula: adodge,
              category: "defence",
              targets: "Auto Dodge"
            }
            data.data.defence = true;
          }
          if (disarm != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            data.data.defence = true;
          }
          if (entangle != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            data.data.defence = true;
          }
          if (damage != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            data.data.damage = true;
          }
          if (pullpunch != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(pullpunch),
              formula: pullpunch,
              category: "defence",
              targets: "Pull"
            }
            data.data.defence = true;
          }
          if (rollpunch != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(rollpunch),
              formula: rollpunch,
              category: "defence",
              targets: "Roll"
            }
            data.data.defence = true;
          }

          if (!data.data.entries[1]) break;
          const item = this.actor.data.items.find(i => i.name === name);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Ranged": {
          // Ranged: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{burst}}  ### {{called}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[R]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          // the value for strike is derived from the Aim total
          const strike = (result[4] != "0") ? Number(result[4]) - 2 : 0;
          const burst = result[5];
          const called = result[6];
          const disarm = result[7];
          const entangle = result[8];
          const damage = result[9] || "0";
          const notes = `Level: ${result[10]}`;

          let nextEntry = 1;
          let data = {
            name: name,
            type: "Modifier",
            data: {
              chartype: "CharacterD120",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };
          if (initiative != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            data.data.defence = true;
          }
          if (actions != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            data.data.primary = true;
          }
          if (strike != 0) {
            data.data.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Ranged"
            }
            data.data.attack = true;
          }
          if (burst != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(burst),
              formula: burst,
              category: "primary",
              targets: "Burst"
            }
            data.data.primary = true;
          }
          if (called != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(called),
              formula: called,
              category: "primary",
              targets: "Called"
            }
            data.data.primary = true;
          }
          if (disarm != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            data.data.defence = true;
          }
          if (entangle != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            data.data.defence = true;
          }
          if (damage != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            data.data.damage = true;
          }

          if (!data.data.entries[1]) break;
          const item = this.actor.data.items.find(i => i.name === name);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
      }
    }
    await this.actor.update({ 'data.biography': biography });
    await this.actor.update({ 'data.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.rtw.${dataset.type}`, { name: dataset.name });
    let threat = Number(dataset.threat) || 12;
    let fail = 2;
    let canCrit = false, isModList = false;
    let useFAHR = this.actor.data.data.useFAHR;
    let rolledInitiative = dataset.name == "Initiative";
    let hideTotal = false;

    switch (dataset.type) {
      case "technique": { // send the notes to the chat and return
        ChatMessage.create(
          {
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            flavor: flavour,
            content: dataset.roll,
          }
        );
        return;
      }
      case "rms": { // no roll, just storing a value
        return;
      }
    }

    // get the modifiers
    let modList = this.fetchRelevantModifiers(this.actor.data, dataset);
    // process the modifiers
    let modformula = "";
    flavour += (dataset.type != "modlist") ? ` [<b>${dataset.roll}</b>]` : `<p class="chatmod">[<b>${dataset.roll}</b>]: ${dataset.name}<br>`;
    let hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (dataset.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    var formula = "";
    switch (dataset.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        isModList = true;
      case "block": // send the value to the chat as a modifiable roll
      case "damage": {// roll damage using a valid dice expression
        formula = dataset.roll;
        break;
      }
      case "check": { // a valid dice expression
        formula = dataset.roll;
        break;
      }
      case "parry": // 2d6 rolls not subject to crits
      case "spell": {
        formula = "2d6 + " + dataset.roll;
        break;
      }
      case "attack": // 2d6 rolls subject to crits
      case "dodge":
      case "skill": {
        formula = "2d6 + " + dataset.roll;
        canCrit = true;
        break;
      }
      default: { // there should not be any of these
        console.error("Failed to process this roll type: " + dataset.type);
      }
    }

    // process the modified roll
    let r = await new Roll(formula + modformula).evaluate({ async: true });

    if (isModList) { // render the modlist dialog and return
      flavour += `<hr><p>${r.result} = <b>${r.total}</b></p>`;
      new Dialog({
        title: this.actor.name,
        content: flavour,
        buttons: {
          close: {
            icon: "<i class='fas fa-tick'></i>",
            label: "Close"
          },
        },
        default: "close"
      }).render(true)
      return;
    }

    if (canCrit) { // an attack or defence roll
      let critfail = false;
      let critsuccess = false;
      if (r.terms[0].total <= fail) {
        critfail = true;
      } else if (r.terms[0].total >= threat) {
        critsuccess = true;
      }
      flavour += ` <p>2D6 Roll: [<b>${r.terms[0].total}</b>]`;
      if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`
    }

    // prepare the flavor in advance based on which type of die is in use.
    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour,
        flags: { hideMessageContent: hideTotal }
      }
    );
    let updates = { ["data.gmod.value"]: 0 };
    if (rolledInitiative) {
      updates["data.bs.value"] = r.total;
    }
    this.actor.update(updates);
    return;
  }
}
