import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorD20Sheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorD20-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importMM').click(this._onImportMMData.bind(this));
    html.find('.convertToD120').click(this._onConvertToD120.bind(this));
  }

  async _onImportMMData(event) {
    event.preventDefault();
    const scriptdata = this.actor.data.data.itemscript.split(/\r?\n/).map(word => word.trim());
    // set the name and notes first
    await this.actor.update({ 'name': scriptdata[0] });
    let biography = "";

    for (let entry of scriptdata) {
      biography += entry + '\r\n';
      const line = entry.split(":");
      switch (line[0].trim()) {
        case "Hit Dice": {
          let temp = line[1].trim();
          let hd = Number(temp.substring(0, temp.search("d")));
          let hp = Number(temp.substring(temp.search(" \\(") + 2, temp.search(" hp")));
          let data = {
            name: "HD",
            type: "Rollable",
            data: {
              chartype: "CharacterD20",
              category: "check",
              formula: hd,
              notes: entry.trim()
            }
          };
          await this.actor.createEmbeddedDocuments('Item', [data]);
          const item = this.actor.data.items.find(i => i.name === "Hit Points");
          const update = { _id: item._id, 'data.max': hp, 'data.value': hp, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Initiative": {
          await this.actor.update({ 'data.bs.value': Number(line[1]) });
          break;
        }
        case "Speed": {
          await this.actor.update({ 'data.bm.step': Number(line[1].trim().split(" ")[0]) });
          break;
        }
        case "Armor Class": {
          let ac = line[1].trim().substring(0, 2);
          const item = this.actor.data.items.find(i => i.name === "Armour Class");
          const update = { _id: item._id, 'data.formula': ac, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Base Attack/Grapple": {
          let data = {
            type: "Melee-Attack",
            data: {
              chartype: "CharacterD20",
              damage: "",
              notes: entry.trim(),
            }
          };
          let regex = new RegExp(/([A-z ]+)\/([A-z ]+): ?([+0-9]+)\/*([+0-9]+)/gi);
          let result = regex.exec(entry);
          data.name = result[1];
          data.data.formula = result[3];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          data.name = result[2];
          data.data.formula = result[4];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Full Attack": {
          const attacks = line[1].split(" or ").map(word => word.trim());
          let data = {
            data: {
              chartype: "CharacterD20"
            }
          };
          for (let attack of attacks) {
            let regex = new RegExp(/([+0-9A-z ()]+) ([+@\-0-9A-z]+)\/?([+\-0-9]+)*\/?([+\-0-9]+)* (ranged|melee) [\(]([^ \/]+)\/?([0-9]{2})?[\-0-9]*\/?(x[0-9]{1})?[, A-z]*[\)]/gi);
            let result = regex.exec(attack);
            data.data.notes = result[0];
            data.name = result[1];
            data.type = (result[5] == "melee") ? "Melee-Attack" : "Ranged-Attack";
            data.data.damage = result[6];
            data.data.armourDiv = result[7];
            data.data.minST = result[8];
            data.data.formula = result[2];
            await this.actor.createEmbeddedDocuments('Item', [data]);
            if (result[3]) {
              data.data.formula = result[3];
              await this.actor.createEmbeddedDocuments('Item', [data]);
              if (result[4]) {
                data.data.formula = result[4];
                await this.actor.createEmbeddedDocuments('Item', [data]);
              }
            }
          }
          break;
        }
        case "Special Attacks": {
          const feats = line[1].split(",").map(word => word.trim());
          let data = {
            type: "Trait",
            data: {
              chartype: "CharacterD20",
              category: "quirk",
              notes: ""
            }
          };
          for (let feat of feats) {
            data.name = feat;
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Special Qualities": {
          const feats = line[1].split(",").map(word => word.trim());
          let data = {
            type: "Trait",
            data: {
              chartype: "CharacterD20",
              category: "perk",
              notes: ""
            }
          };
          for (let feat of feats) {
            data.name = feat;
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Saves": {
          const saves = line[1].split(",").map(word => word.trim());
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterD20",
            }
          };
          for (let save of saves) {
            let parts = save.split(" ").map(word => word.trim());
            switch (parts[0]) {
              case "Fort": {
                const item = this.actor.data.items.find(i => i.name == "Fortitude Save");
                await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'data.notes': save, 'data.formula': parts[1] }]);
              }
              case "Ref": {
                const item = this.actor.data.items.find(i => i.name == "Reflex Save");
                await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'data.notes': save, 'data.formula': parts[1] }]);
              }
              case "Will": {
                const item = this.actor.data.items.find(i => i.name == "Will Save");
                await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'data.notes': save, 'data.formula': parts[1] }]);
              }
            }
          }
          break;
        }
        case "Abilities": {
          const attributes = line[1].split(",").map(word => word.trim());
          for (let attr of attributes) {
            const item = this.actor.data.items.find(i => i.data.data.abbr == attr.substring(0, 3).toUpperCase());
            await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'data.notes': attr }]);
          }
          break;
        }
        case "Skills": {
          const skills = line[1].split(",").map(word => word.trim());
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD20",
              category: "skill"
            }
          };
          for (let skill of skills) {
            let parts = skill.split("+").map(word => word.trim());
            data.name = parts[0];
            data.data.formula = parts[1];
            data.data.notes = skill;
            const item = this.actor.data.items.find(i => i.name == parts[0]);
            if (item?.id) {
              await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'data.notes': skill, 'data.formula': parts[1] }]);
            } else {
              await this.actor.createEmbeddedDocuments('Item', [data]);
            }
          }
          break;
        }
        case "Feats": {
          const feats = line[1].split(",").map(word => word.trim());
          let data = {
            type: "Trait",
            data: {
              chartype: "CharacterD20",
              category: "advantage",
              notes: ""
            }
          };
          for (let feat of feats) {
            data.name = feat;
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        default: {
          // append the entry to notes
        }
      }
    }
    await this.actor.update({ 'data.biography': biography });
    await this.actor.update({ 'data.itemscript': '' });
    ui.notifications.info("Stat block import complete");
  }

  async _onConvertToD120(event) {
    const updates = [];
    const actorupdate = {};
    const items = this.actor.items.contents;

    if (this.actor.data.img.includes("mystery")) {
      actorupdate.img = "icons/svg/mystery-man-black.svg";
      actorupdate.token = {img: "icons/svg/mystery-man-black.svg"};
    }
    for (let item of items) {
      console.debug(`Converting: ${item.name}, with Id: ${item.id} and Data: `, item.data);
      const data = item.data.toObject();
      data.img = "icons/svg/item-bag.svg";
      data.data.chartype = "CharacterD120";
      switch (data.type) {
        case "Modifier": {
          if (data.data.reaction) {
            data.data.reaction = false;
            data.data.defence = true;
            for (const entry in data.data.entries) {
              if (data.data.entries[entry].category == "reaction") data.data.entries[entry].category = "defence"
              console.log(data.data.entries[entry]);
            }
          }
          break;
        }
        case "Rollable": {
          switch (data.name) {
            case "Armour Class":
            case "Touch Armour Class":
            case "Flatfooted Armour Class": { // change to a Defence, Value to Chat (Value to Share)
              data.type = "Defence";
              data.data.category = "block";
              break;
            }
            default: {
              switch (data.data.category) {
                case "check": { // change to a Value (Reference Value)
                  data.data.category = "rms";
                  break;
                }
                case "spell": { // change to Notes to Chat (Information to Share)
                  data.data.category = "technique";
                  break;
                }
              }
            }
          }
          break;
        }
      }
      updates.push(data);
    }
    await this.actor.updateEmbeddedDocuments("Item", updates);
    const pack = game.packs.get("grpga.vsd-item-dropbox");
    const content = await pack.getContent();
    let delta = "";
    for (let item of content) {
      if (item.name == "D20Pal Items Delta") {
        delta = item.data.data.notes;
        break;
      }
    }
    await this._createItems(delta);
    actorupdate.type = "CharacterD120";
    await this.actor.update(actorupdate);
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.d20.${dataset.type}`, { name: dataset.name });
    let threat = Number(dataset.threat) || 20;
    let fail = 1;
    let isDamageOrReaction = false;

    // spells don't get rolled, just displayed
    if (dataset.type == "spell") {
      ChatMessage.create(
        {
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: flavour,
          content: dataset.details,
        }
      );
      return;
    }

    var formula = "";
    switch (dataset.type) {
      case "damage": {
        formula = dataset.roll;
        flavour += ` [<b>${dataset.roll}</b>]`;
        isDamageOrReaction = true;
        break;
      }
      case "reaction": { // this is Armour Class
        formula = dataset.roll;
        isDamageOrReaction = true;
        break;
      }
      default: { // this is where die type must be selected and where the threat range must be determined
        formula = "1d20 + " + dataset.roll;
        flavour += ` [<b>${dataset.roll}</b>]`;
        break;
      }
    }

    var modList = this.fetchRelevantModifiers(this.actor.data, dataset);

    var hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += `<p class="chatmod">`;
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        formula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    let r = await new Roll(formula).evaluate({ async: true });

    // a Success roll
    if (!isDamageOrReaction) {
      let critfail = false;
      let critsuccess = false;
      if (r.terms[0].total <= fail) {
        critfail = true;
      } else if (r.terms[0].total >= threat) {
        critsuccess = true;
      }

      flavour += ` <p>D20 Roll: [<b>${r.terms[0].total}</b>]`;
      if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`
    }

    // prepare the flavor in advance based on which type of die is in use.
    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour
      }
    );
    this.actor.update({ ["data.gmod.value"]: 0 });
    return;
  }
}
