/**
 * A utility class defining behaviour for ruleset-specific actors
 */
export class ActorRtW {

    prepareAttributes(actorData) {
        const myData = actorData.data;
        const attributeIDs = [];
        for (let [id, trait] of Object.entries(myData.traits)) {
            // value is for references, moddedvalue is the modified ranks
            trait.value = Number(trait.ranks);
            myData.dynamic[trait.abbr] = {
                name: trait.name,
                value: trait.value,
                type: "attribute"
            };
            myData.primaries[trait.abbr] = myData.dynamic[trait.abbr];
            attributeIDs.push(trait.abbr);
        }
        return attributeIDs;
    }

    prepareAdditionalData(actorData) {
        const myData = actorData.data;
        // initialise the secondary stats
        for (let [id, stat] of Object.entries(myData.secondaries)) {
            stat.value = stat.basemax = stat.max = myData.traits[stat.trait].value * 4;
            stat.temp = 0;
        }
        // update points to be applied from the array of Points
        for (let [id, points] of Object.entries(myData.points)) {
            const stat = myData.secondaries[points.data.stat];
            switch (points.data.type) {
                case "damage":
                case "burn":
                case "bind":
                case "spend": {
                    stat.value -= points.data.qty;
                    break;
                }
                case "temp": {
                    let change = 0;
                    change = Math.min(points.data.qty, stat.basemax - stat.temp);
                    stat.temp += change;
                    stat.max += change;
                    stat.value += change;
                    break;
                }
            }
        }
    }
}