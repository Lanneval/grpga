import { ActorRtW } from "./actorRtW.js";
import { ActorMnM } from "./actorMnM.js";
import { ActorD120 } from "./actorD120.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class BaseActor extends Actor {

  /**
   * The helper class for specific rulesets.
   */
  rulesetActor = null;

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareDerivedData() {

    const actorData = this.data;

    actorData.data.ruleset = game.settings.get("grpga", "rulesetChoice"); // 3d6, d20, d120, d100, vsd, fragged, oats

    actorData.data.useFAHR = game.settings.get("grpga", "useFAHR");

    actorData.data.rankMode = game.settings.get("grpga", "rankMode");
    actorData.data.autoCrit = game.settings.get("grpga", "autoCriticalRolls");

    if (CONFIG.grpga.testMode) console.debug("entering prepareDerivedData()\n", [this, actorData]);

    let useDnDAttributeModifierModel = false, useRolemasterDefenses = false;

    const actoritems = actorData.items;
    // for counting the ranks stored in Variable Items for some systems
    let actorranks = {};
    let rankitems = [];
    let hasranks = false;

    const myData = actorData.data;
    myData.defence = {};
    myData.points = {};
    myData.ingredients = {};
    myData.dynamic = {};
    myData.primaries = {};
    myData.modifiers = {};
    myData.tracked = {};

    // group the items for efficient iteration
    const actormods = [];
    const actornonmods = [];
    const actorrollables = [];

    // store references to primary attribute and pool items
    let previousLayer = [];
    let nextLayer = [];

    switch (actorData.type) {
      case "CharacterRtW": this.rulesetActor = new ActorRtW(); break;
      case "CharacterD120": this.rulesetActor = new ActorD120(); break;
      case "CharacterMnM": this.rulesetActor = new ActorMnM(); break;
      default: this.rulesetActor = null;
    }

    switch (actorData.type) {
      case "CharacterD20": {
        useDnDAttributeModifierModel = true;
        myData.mode = "d20"; // used to effect system-specific localisation
        myData.bm.quarter = myData.bm.step * 2;
        myData.bm.half = myData.bm.step * 3;
        myData.bm.move = myData.bm.step * 4;
        break;
      }
      case "Character3D6": {
        myData.mode = "3d6"; // same as above
        break;
      }
      case "CharacterD120": {
        myData.mode = "d120"; // same as above
        // the percentile hijack will count ranks
        if (myData.usePercent) hasranks = myData.ruleset == myData.mode;
        this.rulesetActor.setHijack(actorData);
        break;
      }
      case "CharacterRtW": {
        myData.mode = "rtw"; // same as above
        nextLayer = this.rulesetActor.prepareAttributes(actorData);
        break;
      }
      case "CharacterMnM": {
        myData.mode = "mnm"; // same as above
        hasranks = myData.ruleset == myData.mode;
        break;
      }
      case "CharacterFragged": {
        myData.mode = "fragged"; // same as above
        break;
      }
      case "CharacterOaTS": {
        useDnDAttributeModifierModel = true;
        myData.mode = "oats"; // same as above
        break;
      }
      case "CharacterD100": {
        useRolemasterDefenses = true;
        myData.mode = "d100"; // same as above
        myData.bm.quarter = Number(myData.bm.step * 1.5);
        myData.bm.half = myData.bm.step * 2;
        myData.bm.move = myData.bm.step * 3;
        myData.bm.sprint = myData.bm.step * 4;
        myData.bm.dash = myData.bm.step * 5;
        myData.dynamic.woundsbleed = { "value": 0 };
        myData.dynamic.woundspen = { "value": 0 };
        hasranks = myData.ruleset == myData.mode;
        break;
      }
      case "CharacterVsD": {
        useRolemasterDefenses = true;
        myData.mode = "vsd"; // same as above
        myData.dynamic.woundsbleed = { "value": 0 };
        myData.dynamic.woundspen = { "value": 0 };
        myData.dynamic.visionType = "normal";
        hasranks = myData.ruleset == myData.mode;
        break;
      }
    }

    // check the D120 hijacks to see if they use the D&D modifier model
    useDnDAttributeModifierModel = useDnDAttributeModifierModel || myData.useD20 || myData.use2D10 || myData.use3D6;

    // write the items into the model
    for (const item of actoritems) {
      const itemdata = item.data;
      // collect non-modifier items with formula
      if (itemdata.data.formula) actornonmods.push(itemdata);

      switch (itemdata.type) {
        case "Pool": {
          let itemID = itemdata.data.abbr.toLowerCase();
          itemdata.data.hasmin = false;
          itemdata.data.hasmax = false;

          // write the pool to tracked so it can be seen by the token
          myData.tracked[itemID] = {
            _id: itemdata._id,
            abbr: itemdata.data.abbr,
            name: itemdata.name,
            state: itemdata.data.state,
            min: Number(itemdata.data.min),
            value: Number(itemdata.data.value),
            max: Number(itemdata.data.max),
            notes: itemdata.data.notes,
            type: itemdata.type
          };
          if (itemID) actornonmods.push(itemdata);
          break;
        }
        case "Modifier": {
          let itemID = this.slugify(itemdata.name);
          myData.modifiers[itemID] = itemdata;
          actormods.push(itemdata);
          itemdata.data.tempName = itemdata.name;
          break;
        }
        case "Points": {
          myData.points[itemdata._id] = itemdata;
          break;
        }
        case "Ingredient": {
          myData.ingredients[itemdata._id] = itemdata;
          break;
        }
        case "Melee-Attack":
        case "Ranged-Attack": {
          // write these items to dynamic
          let itemID = this.slugify(itemdata.name);
          myData.dynamic[itemID] = {
            name: itemdata.name,
            value: Number(itemdata.data.value),
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };
          if (Number.isNumeric(itemdata.data.formula)) nextLayer.push(itemID);
          actorrollables.push(itemdata);
          break;
        }
        case "Defence":
        case "Rollable": {
          // write these items to dynamic
          let itemID = this.slugify(itemdata.name);
          myData.dynamic[itemID] = {
            name: itemdata.name,
            formula: itemdata.data.formula,
            category: itemdata.data.category,
            value: Number(itemdata.data.value),
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };

          // set an actorranks value of 0 for each Rollable and Defence
          actorranks[itemID] = 0;

          if (Number.isNumeric(itemdata.data.formula) || itemdata.data.formula.startsWith("#")) nextLayer.push(itemID);
          actorrollables.push(itemdata);
          break;
        }
        case "Hit-Location": {
          switch (myData.mode) {
            case "vsd":
            case "d100": {
              // summary of wound effects
              myData.dynamic.woundsbleed.value += itemdata.data.damageResistance;
              myData.dynamic.woundspen.value += itemdata.data.damage;
            }
          }
          break;
        }
        case "Variable": {
          // write these items to dynamic
          let itemID = this.slugify(itemdata.name);
          myData.dynamic[itemID] = {
            name: itemdata.name,
            value: Number(itemdata.data.value),
            formula: itemdata.data.formula,
            label: itemdata.data.label,
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };
          // add the term to the layer if the formula is a number
          if (Number.isNumeric(itemdata.data.formula)) nextLayer.push(itemID);

          // add any items whose name starts with "Ranks:" to the list to be polled next
          if (itemdata.name.startsWith("Ranks:")) rankitems.push(itemdata);

          break;
        }
        case "Primary-Attribute": {
          itemdata.data.moddedvalue = Number(itemdata.data.attr);

          if (myData.useSineNomine) {
            if (myData.use2D6) { // WWN / SWN
              switch (Number(itemdata.data.moddedvalue)) {
                case 3:
                  itemdata.data.value = -2; break;
                case 4:
                case 5:
                case 6:
                case 7:
                  itemdata.data.value = -1; break;
                case 14:
                case 15:
                case 16:
                case 17:
                  itemdata.data.value = 1; break;
                case 18:
                  itemdata.data.value = 2; break;
                default:
                  itemdata.data.value = 0; break;
              }
            } else { // Godbound
              switch (Number(itemdata.data.moddedvalue)) {
                case 3:
                  itemdata.data.value = -3; break;
                case 4:
                case 5:
                  itemdata.data.value = -2; break;
                case 6:
                case 7:
                case 8:
                  itemdata.data.value = -1; break;
                case 13:
                case 14:
                case 15:
                  itemdata.data.value = 1; break;
                case 16:
                case 17:
                  itemdata.data.value = 2; break;
                case 18:
                  itemdata.data.value = 3; break;
                case 19:
                  itemdata.data.value = 4; break;
                default:
                  itemdata.data.value = 0; break;
              }
            }
          } else if (useDnDAttributeModifierModel) {
            itemdata.data.value = Math.floor((Number(itemdata.data.moddedvalue) - 10) / 2);
          } else {
            itemdata.data.value = Number(itemdata.data.moddedvalue);
          }

          // write name and value of the item to dynamic
          let itemID = this.slugify(itemdata.data.abbr);
          myData.dynamic[itemID] = {
            name: itemdata.data.abbr,
            moddedvalue: itemdata.data.moddedvalue,
            value: itemdata.data.value,
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };

          // write the item reference to primaries so you may modifiy it later
          myData.primaries[itemID] = itemdata;

          // add the term to the layer
          if (itemID) nextLayer.push(itemID);
          break;
        }
        default: { // Traits that define a vision type for the actor
          switch (itemdata.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              myData.dynamic.visionType = itemdata.name;
            }
          }
        }
      }
    }

    if (hasranks) { // calculate the ranks, then process number formulae
      if (CONFIG.grpga.testMode) console.debug("Rankitems to be polled:\n", [this, rankitems]);

      // poll the rank items for systems which count them
      for (const itemdata of rankitems) {
        for (let entry of Object.values(itemdata.data.entries)) {
          let target = this.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorranks[target] = (actorranks[target]) ? actorranks[target] + entry.value : entry.value;
          }
        }
      }

      if (CONFIG.grpga.testMode) console.debug("Rankdata after polling:\n", [this, actorranks]);

      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorranks)) {
        if (myData.dynamic[name]) {

          // create a name-ranks entry in dynamic so its value can be used as a formula reference
          myData.dynamic[`${name}-ranks`] = {
            name: `${myData.dynamic[name].name}-Ranks`,
            value: ranks
          };

          // calculate rank bonus to assign to value and moddedvalue
          let value = this.rankCalculator(ranks);
          myData.dynamic[name].formula = `#${ranks}`;
          myData.dynamic[name].ranks = ranks;
          myData.dynamic[name].value = value;
          myData.dynamic[name].moddedvalue = value;
        } else {
          if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }

      // if the formula is a number, process it now
      for (const itemdata of actornonmods) {
        let dynID = this.slugify(itemdata.name);
        switch (itemdata.type) {
          case "Defence":
          case "Rollable": {
            // write to Actor Item???
            itemdata.data.formula = myData.dynamic[dynID].formula;
            itemdata.data.value = myData.dynamic[dynID].value;
            break;
          }
          default: {
            if (Number.isNumeric(itemdata.data.formula)) {
              // write to Actor Item
              itemdata.data.value = Number(itemdata.data.formula);
              // write to dynamic
              myData.dynamic[dynID].value = itemdata.data.value;
            }
          }
        }
      }
    } else { // straight to number formula processing
      for (const itemdata of actornonmods) {
        if (itemdata.type == "Pool") { // new formula behaviour for min and max
          let dynID = itemdata.data.abbr.toLowerCase();
          if (Number.isNumeric(itemdata.data.maxForm)) {
            // write to Actor Item
            itemdata.data.max = Number(itemdata.data.maxForm);
            // write to tracked
            myData.tracked[dynID].max = itemdata.data.max;
            itemdata.data.hasmax = true;
          }
          if (Number.isNumeric(itemdata.data.minForm)) {
            // write to Actor Item
            itemdata.data.min = Number(itemdata.data.minForm);
            // write to tracked
            myData.tracked[dynID].min = itemdata.data.min;
            itemdata.data.hasmin = true;
          }
          if (itemdata.data.hasmax && itemdata.data.hasmin) nextLayer.push(dynID);
        } else {
          if (Number.isNumeric(itemdata.data.formula)) {
            let dynID = this.slugify(itemdata.name);
            // write to Actor Item
            itemdata.data.value = Number(itemdata.data.formula);
            // write to dynamic
            myData.dynamic[dynID].value = itemdata.data.value;
          }
        }
      }
    }

    // Primary Attribute modifiers first
    for (const itemdata of actormods) {
      // the modifier has a primod and is in effect
      if (itemdata.data.primary && itemdata.data.inEffect) {
        for (let entry of Object.values(itemdata.data.entries)) {
          // this entry is a primod
          if (entry.category == "primary") {
            // process primods formulae before the mods are applied
            if (Number.isNumeric(entry.formula)) {
              entry.value = Number(entry.formula);
            } else {
              if (entry.formula == "") continue;
              let formData = this._replaceData(entry.formula);
              try {
                entry.value = Math.round(eval(formData.value.replace(CONFIG.grpga.dataRgx, '')));
                entry.moddedformula = "" + entry.value; // store the result as a String
              } catch (err) {
                // store the formula ready to be rolled
                entry.moddedformula = formData.value;
                entry.value = 0; // eliminate any old values
                console.debug("Primary Modifier formula evaluation error:\n", [entry.moddedformula]);
              }
              if (!itemdata.data.tempName.includes(formData.label)) itemdata.data.tempName += formData.label;
            }
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            switch (actorData.type) {
              case "CharacterRtW": {
                for (const target of cases) {
                  // we have found the target in dynamic
                  if (myData.dynamic[target]) {

                    // write to traits
                    let origitem = myData.traits[target];
                    origitem.value = Number(origitem.value) + entry.value;

                    // write to dynamic
                    myData.dynamic[target].value = origitem.value;
                  }
                }
                break;
              }
              default: {
                for (const target of cases) {
                  // we have found the target in dynamic
                  if (myData.dynamic[target]) {

                    // write to primaries
                    let origitem = myData.primaries[target];
                    origitem.data.moddedvalue = Number(origitem.data.moddedvalue) + entry.value;
                    origitem.data.value = (useDnDAttributeModifierModel) ? Math.floor((origitem.data.moddedvalue - 10) / 2) : origitem.data.moddedvalue;

                    // write to dynamic
                    myData.dynamic[target].moddedvalue = Number(myData.dynamic[target].moddedvalue) + entry.value;
                    myData.dynamic[target].value = (useDnDAttributeModifierModel) ? Math.floor((myData.dynamic[target].moddedvalue - 10) / 2) : myData.dynamic[target].moddedvalue;
                  }
                }
              }
            }
          }
        }
      }
    }

    switch (actorData.type) {
      case "CharacterRtW":
      case "CharacterD120":
      case "CharacterMnM":
        this.rulesetActor.prepareAdditionalData(actorData);
        break;
      default:
        break;
    }

    // calculate the remaining formulae one layer at a time
    while (nextLayer.length !== 0) {
      previousLayer = nextLayer;
      nextLayer = [];

      for (const itemdata of actornonmods) {

        if (itemdata.type == "Pool") { // new pool formula functionality
          //ignore numeric formulae
          if (itemdata.data.hasmax && itemdata.data.hasmin) continue;

          let dynID = itemdata.data.abbr.toLowerCase();

          if (!itemdata.data.hasmax) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.data.maxForm.includes(target)) {
                // write to Actor Item
                let formData = this._replaceData(itemdata.data.maxForm);
                try {
                  itemdata.data.max = Math.round(eval(formData.value.replace(CONFIG.grpga.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.data.max = 0; // eliminate any old values
                  console.error("Pool-max formula evaluation error:\n", [itemdata.data.maxForm]);
                }
                // write to tracked
                myData.tracked[dynID].max = itemdata.data.max; // this pushes a new value to the target, not the item
                itemdata.data.hasmax = true;
              }
            }
          }
          if (!itemdata.data.hasmin) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.data.minForm.includes(target)) {
                // write to Actor Item
                let formData = this._replaceData(itemdata.data.minForm);
                try {
                  itemdata.data.min = Math.round(eval(formData.value.replace(CONFIG.grpga.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.data.min = 0; // eliminate any old values
                  console.error("Pool-min formula evaluation error:\n", [itemdata.data.minForm]);
                }
                // write to tracked
                myData.tracked[dynID].min = itemdata.data.min; // this pushes a new value to the target, not the item
                itemdata.data.hasmin = true;
              }
            }
          }
          if (itemdata.data.hasmax && itemdata.data.hasmin) nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.data.formula || Number.isNumeric(itemdata.data.formula) || itemdata.data.formula.startsWith("#") || Number(itemdata.data.moddedformula) == itemdata.data.value) continue;

          for (const target of previousLayer) {

            // if this formula refers to an item in the previous layer then process it
            if (itemdata.data.formula.includes(target)) {

              let dynID = this.slugify(itemdata.name);

              // write to Actor Item
              let formData = this._replaceData(itemdata.data.formula);
              try {
                itemdata.data.value = Math.round(eval(formData.value.replace(CONFIG.grpga.dataRgx, '')) * 100) / 100;
                itemdata.data.moddedformula = "" + itemdata.data.value; // store the result as a String
              } catch (err) {
                // store the formula ready to be rolled
                itemdata.data.moddedformula = formData.value;
                itemdata.data.value = 0; // eliminate any old values
                console.debug("Non-Modifier formula evaluation error:\n", [itemdata.data.moddedformula]);
              }

              // write to dynamic
              myData.dynamic[dynID].value = itemdata.data.value; // this pushes a new value to the target, not the item

              // put this item into the next layer
              nextLayer.push(dynID);
            }
          }
        }
      }
    }

    // process all modifiers again after the non-modifiers have been processed
    for (const itemdata of actormods) {
      for (let entry of Object.values(itemdata.data.entries)) {
        if (Number.isNumeric(entry.formula)) {
          entry.value = Number(entry.formula);
        } else {
          if (entry.formula == "") continue;
          let formData = this._replaceData(entry.formula);
          try {
            entry.value = Math.round(eval(formData.value.replace(CONFIG.grpga.dataRgx, '')));
            entry.moddedformula = "" + entry.value; // store the result as a String
          } catch (err) {
            // store the formula ready to be rolled
            entry.moddedformula = formData.value;
            entry.value = 0; // eliminate any old values
            console.debug("Modifier formula evaluation error:\n", [entry.moddedformula]);
          }
          if (!itemdata.data.tempName.includes(formData.label)) itemdata.data.tempName += formData.label;
        }
      }
    }

    // calculate the modified value of the item
    for (const itemdata of actorrollables) {
      itemdata.data.moddedvalue = itemdata.data.value;
      // if there is no modified formula, make it the value
      if (itemdata.data.moddedformula == undefined) itemdata.data.moddedformula = "" + itemdata.data.value;
      let itemID = this.slugify(itemdata.name);
      switch (itemdata.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "attack");
          break;
        }
        case "Defence": {
          itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "defence");
          break;
        }
        case "Rollable": {
          switch (itemdata.data.category) {
            case "check": {
              itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "reaction");
              break;
            }
            case "technique":
            case "skill": {
              itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "skill");
              break;
            }
            case "rms":
            case "spell": {
              itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "spell");
              break;
            }
          }
          break;
        }
      }
      myData.dynamic[itemID].moddedvalue = itemdata.data.moddedvalue;
    }

    // DB calculations for Rolemaster-family games
    if (useRolemasterDefenses) {
      myData.bs.value = (myData.dynamic["declared-action"]) ? myData.dynamic["declared-action"].value : myData.bs.value;
      myData.defence = {
        armourType: myData.dynamic["armor-type"]?.formula || "",
        meleeDB: myData.dynamic["melee-db"]?.moddedvalue || 0,
        missileDB: myData.dynamic["missile-db"]?.moddedvalue || 0,
        parry: myData.dynamic.parry?.value || 0,
        MeSh: 0,
        MiSh: 0
      };
      for (const item of actormods) {
        if (!item.data.inEffect) continue;
        if (item.name?.includes("Shield")) {
          for (let entry of Object.values(item.data.entries)) {
            if (!entry.category == "reaction") continue;
            if (entry.targets.includes("Melee")) myData.defence.MeSh = entry.value;
            if (entry.targets.includes("Missile")) myData.defence.MiSh = entry.value;
          }
          break;
        }
      }
    }
  }

  async getNamedItem(name) {
    let data = this.data.data;
    let parts = name.split(".");
    if (parts.length != 2) return undefined;
    return await this.items.get(data[parts[0]][parts[1]]?._id);
  }

  slugify(text) {
    return text
      .toString()                     // Cast to string
      .toLowerCase()                  // Convert the string to lowercase letters
      .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim()                         // Remove whitespace from both sides of a string
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-');        // Replace multiple - with single -
  }

  rankCalculator(ranks) {
    switch (this.data.data.rankMode) {
      case "0521h": {
        if (ranks > 30) {
          return 80 + (ranks - 30) / 2;
        } else if (ranks > 20) {
          return 70 + (ranks - 20);
        } else if (ranks > 10) {
          return 50 + (ranks - 10) * 2;
        } else if (ranks > 0) {
          return ranks * 5;
        } else {
          return -25;
        }
      }
      case "05321": {
        if (ranks > 30) {
          return 100 + (ranks - 30);
        } else if (ranks > 20) {
          return 80 + (ranks - 20) * 2;
        } else if (ranks > 10) {
          return 50 + (ranks - 10) * 3;
        } else if (ranks > 0) {
          return ranks * 5;
        } else {
          return -25;
        }
      }
      case "0321h": {
        if (ranks > 30) {
          return 60 + (ranks - 30) / 2;
        } else if (ranks > 20) {
          return 50 + (ranks - 20);
        } else if (ranks > 10) {
          return 30 + (ranks - 10) * 2;
        } else if (ranks > 0) {
          return ranks * 3;
        } else {
          return -15;
        }
      }
      case "0521":
        if (ranks == 0) return -25;
      case "1521":
        if (ranks == 0) return -15;
      case "521": {
        if (ranks > 20) {
          return 70 + ranks - 20;
        } else if (ranks > 10) {
          return 50 + (ranks - 10) * 2;
        } else {
          return ranks * 5;
        }
      }
      default: { // 111
        return ranks;
      }
    }
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = /[@|#]([\w.\-]+)/gi;
    let data = this.data.data;
    let tempName = "";
    let findTerms = (match, term) => {
      switch (match[0]) {
        case "@": {
          let value = data.dynamic[term]?.value || data.tracked[term]?.value;
          let label = data.dynamic[term]?.label;
          tempName += (label) ? ` (${label})` : "";
          return (value) ? String(value).trim() : "0";
        }
        case "#": {
          return this.rankCalculator(Number(term));
        }
      }
    };

    let replyData = formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)/g, "Math.$&");
    return { value: replyData, label: tempName };
  }

  async setConditions(newValue, attrName) {
    if (CONFIG.grpga.testMode) console.debug("entering setConditions()\n", [newValue, attrName]);

    const tracked = this.data.data.tracked;
    const attr = attrName.split('.')[2];
    const item = this.items.get(tracked[attr]._id);
    const itemData = item.data.data;
    let attrValue = itemData.value;
    let attrMax = itemData.max;
    let attrState = itemData.state;
    let attrMin = itemData.min;

    // Assign the variables
    if (attrName.includes('.max')) {
      attrMax = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.grpga.dataRgx, '')));
    } else if (attrName.includes('.min')) {
      attrMin = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.grpga.dataRgx, '')));
    } else {
      attrValue = newValue;
    }
    const ratio = attrValue / attrMax;

    switch (attr) {
      case "vit": { // Star Wars D20
        if (attrValue < 1) attrState = '[EXHSTD]';
        else if (ratio < 0.25) attrState = '[BUSHED]';
        else if (ratio < 0.5) attrState = '[TIRED]';
        else if (ratio < 0.75) attrState = '[WINDED]';
        else attrState = '[FIT]';
        break;
      }
      case "wnd": { // Star Wars D20
        if (attrValue < -9) attrState = '[DEAD]';
        else if (attrValue < 1) attrState = '[UNC]';
        else if (ratio < 0.25) attrState = '[WND]';
        else if (ratio < 0.5) attrState = '[BLD]';
        else if (ratio < 0.75) attrState = '[BRU]';
        else attrState = '[FIT]';
        break;
      }
      case "hp": {
        switch (this.data.type) {
          case "CharacterD20": { // health state for D20 system
            if (attrValue < -9) attrState = '[DEAD]';
            else if (attrValue < 1) attrState = '[UNC]';
            else if (ratio < 0.25) attrState = '[WND]';
            else if (ratio < 0.5) attrState = '[BLD]';
            else if (ratio < 0.75) attrState = '[BRU]';
            else attrState = '[FIT]';
            break;
          }
          case "CharacterVsD": { // health state for Against the Darkmaster
            if (this.data.data.useFAHR) { // House rules
              switch (Math.trunc(ratio * 8)) {
                case 1: {
                  attrState = '[1/8 BRU]';
                  break;
                }
                case 2: {
                  attrState = '[1/4 BRU]';
                  break;
                }
                case 3: {
                  attrState = '[3/8 BRU]';
                  break;
                }
                case 4: {
                  attrState = '[1/2]';
                  break;
                }
                case 5: {
                  attrState = '[5/8]';
                  break;
                }
                case 6: {
                  attrState = '[3/4]';
                  break;
                }
                case 7: {
                  attrState = '[7/8]';
                  break;
                }
                case 8: {
                  attrState = '[FIT]';
                  break;
                }
                default: { // bad shape
                  if (ratio <= -1) {
                    attrState = '[DEAD]';
                  } else if (ratio <= -0.5) {
                    attrState = '[DYING]';
                  } else if (ratio <= 0) {
                    attrState = '[INC]';
                  } else {
                    attrState = '[< 1/8 BRU]';
                  }
                }
              }
            } else { // RAW
              if (attrValue < -49) attrState = '[DYING]';
              else if (attrValue < 1) attrState = '[INC]';
              else if (ratio < 0.5) attrState = '[BRU]';
              else attrState = '[FIT]';
            }
            break;
          }
          case "Character3D6": { // health state for 3D6 ruleset
            switch (Math.trunc(ratio)) {
              case 0: {
                if (ratio <= 0) { // collapse
                  attrState = '[C]';
                  break;
                } else if (attrValue < (attrMax / 3)) { // reeling
                  attrState = '[R]';
                  break;
                }
                // healthy, no break
              }
              case 1: { // healthy
                attrState = '[H]';
                break;
              }
              case -1: { // death check at -1
                attrState = '[-X]';
                break;
              }
              case -2: { // death check at -2
                attrState = '[-2X]';
                break;
              }
              case -3: { // death check at -3
                attrState = '[-3X]';
                break;
              }
              case -4: { // death check at -4
                attrState = '[-4X]';
                break;
              }
              default: { // dead
                attrState = '[DEAD]';
                break;
              }
            }
            break;
          }
          default: { // use eighths and let the user set the minimum
            switch (Math.trunc(ratio * 8)) {
              case 1: {
                attrState = '[1/8]';
                break;
              }
              case 2: {
                attrState = '[1/4]';
                break;
              }
              case 3: {
                attrState = '[3/8]';
                break;
              }
              case 4: {
                attrState = '[1/2]';
                break;
              }
              case 5: {
                attrState = '[5/8]';
                break;
              }
              case 6: {
                attrState = '[3/4]';
                break;
              }
              case 7: {
                attrState = '[7/8]';
                break;
              }
              case 8: {
                attrState = '[FIT]';
                break;
              }
              default: { // bad shape
                if (attrValue <= attrMin) {
                  attrState = '[DEAD]';
                } else if (attrValue <= 0) {
                  attrState = '[UNC]';
                } else {
                  attrState = '[< 1/8]';
                }
              }
            }
            break;
          }
        }
        break;
      }
      case "fp": {
        // set the limits
        switch (Math.trunc(ratio)) {
          case 0: {
            if (ratio <= 0) { // collapse
              attrState = '[C]';
              break;
            } else if (attrValue < (attrMax / 3)) { // tired
              attrState = '[T]';
              break;
            }
            // fresh, no break
          }
          case 1: { // fresh
            attrState = '[F]';
            break;
          }
          default: { // unconscious
            attrState = '[UNC]';
            break;
          }
        }
        break;
      }
      default: { // assume that the state is measured in eighths
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = '[1/8]';
            break;
          }
          case 2: {
            attrState = '[1/4]';
            break;
          }
          case 3: {
            attrState = '[3/8]';
            break;
          }
          case 4: {
            attrState = '[1/2]';
            break;
          }
          case 5: {
            attrState = '[5/8]';
            break;
          }
          case 6: {
            attrState = '[3/4]';
            break;
          }
          case 7: {
            attrState = '[7/8]';
            break;
          }
          case 8: {
            attrState = '[Full]';
            break;
          }
          default: { // dead
            if (ratio <= 0) { // empty
              attrState = '[Empty]';
            } else {
              attrState = '[< 1/8]';
            }
          }
        }
      }
    }
    itemData.min = attrMin;
    itemData.value = attrValue;
    itemData.max = attrMax;
    itemData.state = attrState;

    return await item.update({ data: itemData });
  }

  // a test method for executing token actions. SysDev part 8 covers a better way to do this.
  async attack() {
    ui.notifications.info("You have triggered the attack method of this actor.");
    const attacks = this.data.items.filter(i => (i.type == "Melee-Attack" || i.type == "Ranged-Attack"));
    const attackdata = [];
    for (const attack of attacks) {
      attackdata.push(attack.data);
    }

    /**
        let attackscript = `<form><div id="attacks">`;
    
        for (const item of attacks) {
          const itemdata = item.data;
          attackscript +=
            `<div class="attack" data-item-id=${itemdata._id} title="${itemdata.data.notes}">
            <label class="rollable" data-name="${itemdata.name}" data-roll=${itemdata.data.value} data-type="attack">
              [${itemdata.data.value}] ${itemdata.name}
            </label>
            <label class="rollable" data-name="${itemdata.name}" data-roll="${itemdata.data.damage}" data-damageType="${itemdata.data.damageType}" data-armourdiv=${itemdata.data.armourDiv} data-type="damage">
              ${itemdata.data.damage} ${itemdata.data.damageType}
            </label>
          </div>
        </div></form>`
        }
    
        attackscript += `</div>`;
    */
    const template = "systems/grpga/templates/dice/attacks.hbs";
    const html = await renderTemplate(template, { attackdata });

    //html.find('.rollable').click(this.sheet._onRoll.bind(this));

    new Dialog({
      title: "Attacks",
      content: html,
      buttons: {}
    }).render(true);
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * Logic for pools to be edited by plus-minus and text input (on Actor and Item) is also redirected here.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    if (CONFIG.grpga.testMode) console.debug("entering modifyTokenAttribute()\n", [attribute, value, isDelta, isBar]);

    // fetch the pool if the attribute changing is 'value', or the attribute itelf if not
    const current = getProperty(this.data.data, attribute);
    // isBar is true for the value and must be clamped
    // isBar is false for the min or max and can only 
    value = (isBar) ? (Math.clamped(current.min, (isDelta) ? Number(current.value) + Number(value) : Number(value), current.max)) : ((isDelta) ? Number(current) + Number(value) : value);

    // redirect updates to setConditions
    return await this.setConditions(value, `data.${attribute}`);
  }

  /**
   * Fetches the total modifier value for a rollable item
   */
  fetchRelevantModifiers(name, type) { // only attack, defence, reaction, skill & spell
    let tempMods = Number(this.data.data.gmod.value);
    const actormods = this.data.items.filter(i => (i.type == "Modifier"));

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.data;

      if (moddata.data.inEffect) {

        for (let entry of Object.values(moddata.data.entries)) {
          // check to see if this entry applies to this type of roll
          if (type != entry.category) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }
          tempMods += (hasRelevantTarget ? entry.value : 0);
        }
      }
    }
    return tempMods;
  }

  /**
   * Resets all temporary Variables and Modifiers
   */
  resetTemporaryItems() {
    const temporaryItems = this.data.items.filter(i => (i.data.data.temporary == true));
    let updates = [];

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const item of temporaryItems) {
      switch (item.type) {
        case "Modifier": {
          updates.push({
            _id: item.id,
            ["data.inEffect"]: false
          });
          break;
        }
        case "Variable": {
          updates.push({
            _id: item.id,
            ["data.value"]: item.data.data.entries[1].value,
            ["data.formula"]: item.data.data.entries[1].formula,
            ["data.label"]: item.data.data.entries[1].label
          });
          break;
        }
      }
    }
    this.updateEmbeddedDocuments("Item", updates);
  }
}

/**
 * Counting the ranks listed in Variable items whose names start with "Ranks: "
 */
export const countRanks = function (actor) {
  if (CONFIG.grpga.testMode) console.debug("Counting Ranks");
  let actoritems = actor.data.items;
  let actorranks = {};
  let rankitems = [];

  for (const item of actoritems) {
    const itemdata = item.data
    switch (itemdata.type) {
      // add any items whose name starts with "Ranks:" to the list to be polled next
      case "Variable": {
        if (itemdata.name.startsWith("Ranks:")) rankitems.push(itemdata);
        break;
      }
      // poll all items and set an actorranks value of 0 for each Rollable and Defence
      case "Defence":
      case "Rollable": {
        let slug = actor.slugify(itemdata.name);
        actorranks[slug] = 0;
        break;
      }
    }
  }

  if (CONFIG.grpga.testMode) console.debug("Rankitems to be polled:\n", [actor, rankitems]);

  // poll the rank items
  for (const itemdata of rankitems) {
    for (let entry of Object.values(itemdata.data.entries)) {
      let target = actor.slugify(entry.label);
      // if the name is not blank
      if (target) {
        entry.value = Number(entry.formula);
        actorranks[target] = (actorranks[target]) ? actorranks[target] + entry.value : entry.value;
      }
    }
  }

  if (CONFIG.grpga.testMode) console.debug("Rankdata after polling:\n", [actor, actorranks]);

  for (const [name, ranks] of Object.entries(actorranks)) {
    if (actor.data.data.dynamic[name]) {
      let item = actor.items.get(actor.data.data.dynamic[name]._id);
      let itemdata = item.data.data;
      // the ranks and formula are both zero. Nothing to do here.
      if (ranks == 0 && itemdata.formula == "#0" && itemdata.moddedvalue == 0) continue;
      // the ranks and formula are already the same. Nothing to do here.
      if (`#${ranks}` == itemdata.formula) continue;

      // calculate rank bonus to assign to value and moddedvalue
      let value = 0;
      let moddedvalue = 0;
      if (ranks > 20) {
        value = 70 + ranks - 20;
      } else if (ranks > 10) {
        value = 50 + (ranks - 10) * 2;
      } else {
        value = ranks * 5;
      }
      let data = {
        "data.formula": `#${ranks}`,
        "data.value": value,
        "data.moddedvalue": moddedvalue
      }
      item.update(data);
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were assigned to : ${name}, which had ${itemdata.formula} ranks`);
      // this seems to cause a bit of recursion but should be more efficient than the item.update which calls it anyway.
      //actor.updateEmbeddedDocuments("Item", { id: actordata.data.dynamic[name].id, 'data.formula': (ranks == 0) ? 0 : `#${ranks}` });
    } else {
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
    }
  }
}

/**
 * Erasing the # symbol from all items in an actor"
 */
export const resetRanks = function (actor) {
  if (CONFIG.grpga.testMode) console.debug("Erasing Ranks");
  let actoritems = actor.data.items;
  let updates = [];

  for (const item of actoritems) {
    const itemdata = item.data
    try { // if the item has a formula and that formula includes #
      if (itemdata.data.formula?.includes("#")) {
        let data = {
          "data.formula": "0",
          "data.value": 0,
          "data.moddedvalue": 0
        }
        item.update(data);
      }
    } catch (err) { // the formula is an integer, replace it
      let data = {
        "data.formula": "0",
        "data.value": 0,
        "data.moddedvalue": 0
      }
      item.update(data);
    }
  }
  return;
  for (const [name, ranks] of Object.entries(actorranks)) {
    if (actor.data.data.dynamic[name]) {
      let item = actor.items.get(actor.data.data.dynamic[name]._id);
      let itemdata = item.data.data;
      // the ranks and formula are both zero. Nothing to do here.
      if (ranks == 0 && itemdata.formula == "#0" && itemdata.moddedvalue == 0) continue;
      // the ranks and formula are already the same. Nothing to do here.
      if (`#${ranks}` == itemdata.formula) continue;

      // calculate rank bonus to assign to value and moddedvalue
      let value = 0;
      let moddedvalue = 0;
      if (ranks > 20) {
        value = 70 + ranks - 20;
      } else if (ranks > 10) {
        value = 50 + (ranks - 10) * 2;
      } else {
        value = ranks * 5;
      }
      let data = {
        "data.formula": `#${ranks}`,
        "data.value": value,
        "data.moddedvalue": moddedvalue
      }
      item.update(data);
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were assigned to : ${name}, which had ${itemdata.formula} ranks`);
      // this seems to cause a bit of recursion but should be more efficient than the item.update which calls it anyway.
      //actor.updateEmbeddedDocuments("Item", { id: actordata.data.dynamic[name].id, 'data.formula': (ranks == 0) ? 0 : `#${ranks}` });
    } else {
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
    }
  }
}
