/**
 * A utility class defining behaviour for ruleset-specific actors
 */
export class ActorD120 {
    
    setHijack(actorData) {
        actorData.data.useD6Pool = CONFIG.grpga.hijack == "UseD6Pool";
        actorData.data.useD20 = CONFIG.grpga.hijack == "UseD20";
        actorData.data.use2D10 = CONFIG.grpga.hijack == "Use2D10";
        actorData.data.use3D6 = CONFIG.grpga.hijack == "Use3D6";
        actorData.data.use2D6 = CONFIG.grpga.hijack == "Use2D6";
        actorData.data.usePercent = CONFIG.grpga.hijack == "UsePercent";
        actorData.data.useAlternity = CONFIG.grpga.hijack == "UseAlternity";

        actorData.data.useSineNomine = CONFIG.grpga.useSineNomine;
    }

    prepareAttributes(actorData) {
        let attributeIDs = [];
        return attributeIDs;
    }
    prepareAdditionalData(actorData) {
    }
}