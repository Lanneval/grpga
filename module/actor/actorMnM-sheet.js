import { grpga } from "../config.js";
import { baseActorSheet } from "./baseActor-sheet.js";
import { xmlToJson, parseXML, json2xml, xml2json } from "../utility.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorMnMSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["mnm", "grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorMnM-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "skill" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.grpga.testMode) console.debug("entering getData() in ActorMnM-sheet");

    let data = super.getData(options);
    let actor = data.actor;
    let actordata = actor.data;

    data.headerinfo = {};
    let headinfo = data.headerinfo;

    headinfo.level = actordata.dynamic.level;
    headinfo.perception = actordata.dynamic.perception;
    headinfo.initiative = actordata.dynamic.initiative;
    headinfo.dodge = actordata.dynamic.dodge;
    headinfo.xp = actordata.dynamic.xp;
    // for d20
    headinfo.hitpoints = actordata.tracked.hp;
    headinfo.apr = actordata.dynamic.apr;
    headinfo.mpa = actordata.dynamic.mpa;
    headinfo.pace = this.actor.items.get(actordata.dynamic["pace"]?._id)?.data;
    headinfo.mr = actordata.tracked.mr;

    for (let item of data.advantages) {
      switch (item?.name) {
        case "Race": headinfo.race = item; break;
        case "Character Class": headinfo.characterclass = item; break;
        case "Specific Information": headinfo.specificinfo = item; break;
        case "Additional Information": headinfo.additionalinfo = item; break;
        case "Alignment": headinfo.alignment = item; break;
        case "Secret Identity": headinfo.secretName = item; break;
      }
    }

    // group and sort skills correctly for display
    let tempskills = [...data.skills];
    let tempspells = [...data.spells];
    data.skills = [];
    data.techniques = [];
    for (let item of tempskills) {
      if (item.data.category == "technique") {
        data.techniques.push(item);
      } else {
        item.data.stepvalue = item.data.moddedvalue - item.data.value;
        data.skills.push(item);
      }
    }
    data.rms = [];
    for (let item of tempspells) {
      if (item.data.category == "rms") {
        data.rms.push(item);
      } else {
        data.skills.push(item);
      }
    }
    for (let item of data.checks) {
      data.skills.push(item);
    }
    delete data.spells;
    delete data.checks;
    data.skills = this.sort(data.skills);

    // group and sort defences correctly for display
    let tempdefences = [...data.defences];
    data.defences = [];
    data.blocks = [];
    for (let item of tempdefences) {
      if (item.data.category == "block") {
        data.blocks.push(item);
      } else {
        if (data.data.useSineNomine) {
          item.data.value = 16 - item.data.value;
          item.data.moddedvalue = 16 - item.data.moddedvalue;
        } else {
          item.data.stepvalue = item.data.moddedvalue - item.data.value;
        }
        data.defences.push(item);
      }
    }
    data.attackvariables = [];
    for (let varname of grpga.mnm.attackvariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) data.attackvariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
      else if (actordata.tracked[varname]) data.attackvariables.push(this.actor.items.get(actordata.tracked[varname]._id).data);
    }
    data.defencevariables = [];
    for (let varname of grpga.mnm.defencevariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) data.defencevariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
      else if (actordata.tracked[varname]) data.defencevariables.push(this.actor.items.get(actordata.tracked[varname]._id).data);
    }
    data.skillvariables = [];
    for (let varname of grpga.mnm.skillvariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) data.skillvariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
      else if (actordata.tracked[varname]) data.skillvariables.push(this.actor.items.get(actordata.tracked[varname]._id).data);
    }
    data.spellvariables = [];
    for (let varname of grpga.mnm.spellvariables) {
      if (!varname.trim()) continue;
      if (actordata.dynamic[varname]) data.spellvariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
      else if (actordata.tracked[varname]) data.spellvariables.push(this.actor.items.get(actordata.tracked[varname]._id).data);
    }

    // group and sort skill mods
    data.checkskillspellmods = this.sort(Array.from(new Set(data.checkmods.concat(data.skillmods, data.spellmods))));

    delete data.skillmods;
    delete data.checkmods;
    delete data.spellmods;

    // check to see if specific modifiers are toggled so we can set the flags
    // the modifier must affect Variable Formulae, D20 skills or D100 skills to work as a toggle (a modifier of zero is ok)
    // these were designed for D6 Pool ruleset but could be used by others
    for (let item of data.checkskillspellmods) {
      switch (item?.name) {
        case "Rushed": this.actor.setFlag("grpga", "rushed", item.data.inEffect); break;
        case "Using Edge": this.actor.setFlag("grpga", "usingedge", item.data.inEffect); break;
      }
    }

    return data;
  }

  sort(data) {
    return data.sort((a, b) => {
      if (a.sort < b.sort) return -1;
      if (a.sort > b.sort) return 1;
      return 0;
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importText').click(this._onImportTextData.bind(this));
    html.find('.importXML').click(this._onImportXMLData.bind(this));
    html.find('.importHeroLab').click(this._onImportHeroLabData.bind(this));
  }

  async _processJSONData(data) {
    const chardata = data.document.public.character;

    // name
    await this.actor.update({ 'name': chardata.name });

    // personal
    let persdata = {
      personal: {
        age: chardata.personal.age,
        gender: chardata.personal.gender,
        hair: chardata.personal.hair,
        eyes: chardata.personal.eyes,
        description: chardata.personal.description,
        height: chardata.personal.charheight.text,
        weight: chardata.personal.charweight.text
      }
    }
    await this.actor.update({ 'data': persdata });

    // attributes
    let elements = chardata.attributes.attribute;
    let elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let data = {
        name: e.name,
        type: "Primary-Attribute",
        data: {
          chartype: "CharacterMnM",
          attr: Number(e.base),
          cost: Number(e.cost.value)
        }
      }
      await this._updateOrCreateItem(data);
    }

    // advantages
    elements = chardata.advantages.advantage;
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let data = {
        name: e.name,
        type: "Rollable",
        data: {
          chartype: "CharacterMnM",
          category: "technique",
          cost: Number(e.cost.value),
          notes: `<h3>${e.categorytext}</h3><hr><p>${e.description}</p>`
        }
      }
      await this._updateOrCreateItem(data);
    }

    // powers
    elements = chardata.powers.power;
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let data = {
        name: e.name,
        type: "Power",
        data: {
          chartype: "CharacterMnM",
          category: "technique",
          cost: e.cost,
          summary: e.summary,
          notes: `<hr><p>${e.description}</p><p>${e.summary}</p>`
        }
      }
      await this._updateOrCreateItem(data);
    }

    // complications
    elements = chardata.complications.complication;
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let data = {
        name: e.name,
        type: "Trait",
        data: {
          chartype: "CharacterMnM",
          category: "disadvantage",
          notes: `<hr><p>${e.description}</p>`
        }
      }
      await this._updateOrCreateItem(data);
    }
    // languages
    elements = chardata.languages.language;
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let data = {
        name: `Language: ${e.name}`,
        type: "Trait",
        data: {
          chartype: "CharacterMnM",
          category: "disadvantage",
          notes: ""
        }
      }
      await this._updateOrCreateItem(data);
    }
    // defenses
    elements = chardata.defenses.defense;
    elms = elements instanceof Array ? elements : [elements];
    let skillrankdata = {
      name: "Ranks: Defenses",
      type: "Variable",
      data: {
        chartype: "CharacterMnM",
        entries: {
          0: {
            value: 0,
            formula: "",
            label: ""
          }
        }
      }
    }
    let nextIndex = 1;
    for (let e of elms) {
      skillrankdata.data.entries[nextIndex++] = {
        formula: Number(e.cost.value),
        label: e.name
      }
      let skilldata = {
        name: e.name,
        type: "Defence",
        data: {
          chartype: "CharacterMnM",
          category: "dodge",
          formula: "0",
          impervious: Number(e.impervious),
          cost: Number(e.cost.value),
          abbr: e.abbr
        }
      }
      await this._updateOrCreateItem(skilldata);
    }
    await this.actor.createEmbeddedDocuments('Item', [skillrankdata]);
    // skills
    elements = chardata.skills.skill;
    elms = elements instanceof Array ? elements : [elements];
    skillrankdata = {
      name: "Ranks: Skills",
      type: "Variable",
      data: {
        chartype: "CharacterMnM",
        entries: {
          0: {
            value: 0,
            formula: "",
            label: ""
          }
        }
      }
    }
    nextIndex = 1;
    for (let e of elms) {
      let skillname = e.name.split(":")[0].trim();
      let trainedonly = CONFIG.grpga.mnm.trainedonlyskills[skillname];
      skillrankdata.data.entries[nextIndex++] = {
        formula: (e.base != "-") ? Number(e.base) : trainedonly ? "-99" : "0",
        label: e.name
      }
      let skilldata = {
        name: e.name,
        type: "Rollable",
        data: {
          chartype: "CharacterMnM",
          category: "skill",
          formula: "0",
          trainedonly: trainedonly,
          cost: Number(e.cost.value),
          notes: e.description
        }
      }
      await this._updateOrCreateItem(skilldata);
    }
    await this.actor.createEmbeddedDocuments('Item', [skillrankdata]);

  }

  /**
   * For imported and augmented data, detemine if the item exists and:
   * - if so, update the item
   * - if not, create the item
   */
  async _updateOrCreateItem(data) {
    const item = this.actor.data.items.find(i => i.name === data.name);
    if (item) {
      data._id = item.id;
      await this.actor.updateEmbeddedDocuments("Item", [data]);
    } else {
      await this.actor.createEmbeddedDocuments('Item', [data]);
    }
  }

  /**
   * An XML data importer needs:
   * - to read the XML file from the itemscript field
   * - an xml to json converter
   * - a json reader to convert to GRPGA Items
   */
  async _onImportXMLData(event) {
    event.preventDefault();
    let temp = this.actor.data.data.itemscript.replace(/&quot;/g, '#quot;');
    if (temp[0] == "\"") { // remove the wrapping doublequotes
      temp = temp.substr(1, temp.length - 2);
    }
    let data = JSON.parse(xml2json(parseXML(temp), "\t"));
    console.warn("Imported JSON character data\n", data.document.public.character);
    this._processJSONData(data);
  };

  /**
   * A Hero Lab file importer needs:
   * - a file picker to select the .por file
   * - an unzipper to read the xml file in the archive
   * - an xml to json converter
   * - a json reader to convert to GRPGA Items
   */
  async _onImportHeroLabData(event) { };

  async _onImportTextData(event) {
    event.preventDefault();
    let temp = this.actor.data.data.itemscript;
    if (temp[0] == "\"") { // remove the wrapping doublequotes
      temp = temp.substr(1, temp.length - 2);
    }
    const scriptdata = temp.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.data.data.biography;

    let ammoIndex = 0;
    for (let entry of scriptdata) {
      let regex = new RegExp(/([^:]+):([^\r|\n]+)/gi);
      const line = regex.exec(entry);

      switch (line[1].trim()) {
        case "Name": {
          // Name: {{name}}
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
        case "Attribute": {
          // Attribute: {{name}} ### {{abbr || slugify(name)}} ### {{attr || 0}} ### {{sort || 0}} ### {{notes || empty}}
          // Attribute: Intelligence Quotient ### IQ ### 12 ### 1 ### Your ability to reason
          let data = {
            type: "Primary-Attribute",
            data: {
              chartype: "CharacterMnM",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1] || this.actor.slugify(result[0]);
          data.data.attr = Number(result[2]) || 0;
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Skill": {
          // Skill: {{name}} ### {{"VarForm" || "D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          // Skill: Athletics ### D100 ### 45 ### 3 ### Training in vigorous exertion for competition
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          if (!result[1]) {
            data.data.category = "spell";
          } else {
            switch (result[1].toLowerCase()) {
              case "varform":
                data.data.category = "check";
                break;
              case "d20":
                data.data.category = "skill";
                break;
              default:
                data.data.category = "spell";
            }
          }
          data.data.formula = result[2] || "0";
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Information": {
          // Information: {{name}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterMnM",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "You should be putting html formatted content here to be displayed in the chat log.";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Reference": {
          // Reference: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterMnM",
              category: "rms"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Defence": {
          // Defence: {{name}} ### {{"D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          if (!result[1]) {
            data.data.category = "parry";
          } else {
            switch (result[1].toLowerCase()) {
              case "d20":
                data.data.category = "dodge";
                break;
              default:
                data.data.category = "parry";
            }
          }
          data.data.formula = result[2] || "0";
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Trait": {
          // Trait: {{name}} ### {{category}} ### {{notes}} ### {{sort || 0}}
          let data = {
            type: "Trait",
            data: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.category = result[1].toLowerCase();
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "SharedValue": {
          // SharedValue: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterMnM",
              category: "block"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Pool": {
          // Pool: {{name}} ### {{abbr}} ### {{max}} ### {{value}} ### {{min}} ### {{sort || 50}} ### {{notes || empty}}
          let data = {
            type: "Pool",
            data: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1];
          data.data.max = result[2];
          data.data.value = result[3];
          data.data.min = result[4];
          data.data.sort = Number(result[5]) || 50;
          data.data.notes = result[6] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Attack": {
          // Attack: {{name}} ### {{sort || 50}} ### {{range}} ### {{damage}} ### {{rof}} ### {{payload}} ### {{ammo}} ### {{damage class}}
          let data = {
            data: {
              chartype: "CharacterMnM",
              armourDiv: 20,
              minST: "x2"
            }
          }
          let result = line[2].split("###").map(word => word.trim());
          data.data.sort = Number(result[1]) || 50;
          data.data.damage = result[3];
          data.data.damageType = result[7];

          if (result[2] == "0") { // melee
            data.type = "Melee-Attack";
            data.name = `Melee: ${result[0]}`;
            data.data.formula = "@strike-bonus";
          } else { // ranged
            data.type = "Ranged-Attack";
            data.name = `Ranged: ${result[0]}`;
            data.data.formula = "0";
            data.data.accuracy = result[4];
            data.data.range = result[2];
            if (result[5] != "0") { // ammunition pool required
              let ammodata = {
                name: `${result[0]} Ammunition`,
                type: "Pool",
                data: {
                  abbr: `Ammo${++ammoIndex}`,
                  chartype: "CharacterMnM",
                  min: 0,
                  value: Number(result[5]),
                  max: Number(result[5]),
                  notes: `Reloads: ${Number(result[6])}`
                }
              }
              const item = this.actor.data.items.find(i => i.name === ammodata.name);
              if (item) {
                // do not override the existing saved ammunition tracking
              } else {
                await this.actor.createEmbeddedDocuments('Item', [ammodata]);
              }
            }
          }
          const item = this.actor.data.items.find(i => i.name === data.name);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Armor Type": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name === "Armor Type");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Level": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Level-Ranks"));
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Race": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Race"));
          const update = { _id: item.id, 'name': `Race: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "CharacterClass": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Culture"));
          const update = { _id: item.id, 'name': `Culture: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Occupation": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Profession"));
          const update = { _id: item.id, 'name': `Profession: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Psionics": {
          // Psionics: {{name}} ### {{sort || 50}} ### {{ispcost}} ### {{range}} ### {{duration}} ### {{effects}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterMnM",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = "0";
          data.data.sort = Number(result[1]) || 50;
          data.data.notes = `<hr><div><b>ISP Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Effects:</b> ${result[5]}</div>`;
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Magic": {
          // Magic: {{name}} ### {{sort || 50}} ### {{ppecost}} ### {{range}} ### {{duration}} ### {{saves}} ### {{effects}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterMnM",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = "0";
          data.data.sort = Number(result[1]) || 50;
          data.data.notes = `<hr><div><b>PPE Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Saves:</b> ${result[5]}</div><div><b>Effects:</b> ${result[6]}</div>`;
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Melee": {
          // Melee: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{parry}}  ### {{dodge}}  ### {{autododge}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{pullpunch}}  ### {{rollwithpunch}}  ### {{notes  || empty}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[M]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          const strike = result[4];
          const parry = result[5];
          const dodge = result[6];
          const adodge = result[7];
          const disarm = result[8];
          const entangle = result[9];
          const damage = result[10] || "0";
          const pullpunch = result[11];
          const rollpunch = result[12];
          const notes = `${result[13]}\nLevel: ${result[14]}`;

          let nextEntry = 1;
          let data = {
            name: name,
            type: "Modifier",
            data: {
              chartype: "CharacterMnM",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };

          if (initiative != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            data.data.defence = true;
          }
          if (actions != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            data.data.primary = true;
          }
          if (strike != 0) {
            data.data.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Melee"
            }
            data.data.attack = true;
          }
          if (parry != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(parry),
              formula: parry,
              category: "defence",
              targets: "Parry"
            }
            data.data.defence = true;
          }
          if (dodge != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(dodge),
              formula: dodge,
              category: "defence",
              targets: "Dodge"
            }
            data.data.defence = true;
          }
          if (adodge != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(adodge),
              formula: adodge,
              category: "defence",
              targets: "Auto Dodge"
            }
            data.data.defence = true;
          }
          if (disarm != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            data.data.defence = true;
          }
          if (entangle != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            data.data.defence = true;
          }
          if (damage != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            data.data.damage = true;
          }
          if (pullpunch != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(pullpunch),
              formula: pullpunch,
              category: "defence",
              targets: "Pull"
            }
            data.data.defence = true;
          }
          if (rollpunch != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(rollpunch),
              formula: rollpunch,
              category: "defence",
              targets: "Roll"
            }
            data.data.defence = true;
          }

          if (!data.data.entries[1]) break;
          const item = this.actor.data.items.find(i => i.name === name);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Ranged": {
          // Ranged: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{burst}}  ### {{called}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[R]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          // the value for strike is derived from the Aim total
          const strike = (result[4] != "0") ? Number(result[4]) - 2 : 0;
          const burst = result[5];
          const called = result[6];
          const disarm = result[7];
          const entangle = result[8];
          const damage = result[9] || "0";
          const notes = `Level: ${result[10]}`;

          let nextEntry = 1;
          let data = {
            name: name,
            type: "Modifier",
            data: {
              chartype: "CharacterMnM",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };
          if (initiative != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            data.data.defence = true;
          }
          if (actions != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            data.data.primary = true;
          }
          if (strike != 0) {
            data.data.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Ranged"
            }
            data.data.attack = true;
          }
          if (burst != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(burst),
              formula: burst,
              category: "primary",
              targets: "Burst"
            }
            data.data.primary = true;
          }
          if (called != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(called),
              formula: called,
              category: "primary",
              targets: "Called"
            }
            data.data.primary = true;
          }
          if (disarm != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            data.data.defence = true;
          }
          if (entangle != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            data.data.defence = true;
          }
          if (damage != "0") {
            data.data.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            data.data.damage = true;
          }

          if (!data.data.entries[1]) break;
          const item = this.actor.data.items.find(i => i.name === name);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
      }
    }
    await this.actor.update({ 'data.biography': biography });
    await this.actor.update({ 'data.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.mnm.${dataset.type}`, { name: dataset.name });
    let threat = Number(dataset.threat) || 20;
    let fail = 1;
    let canCrit = false, isModList = false, isPercent = false;
    let useFAHR = this.actor.data.data.useFAHR;
    let useD6Pool = this.actor.data.data.useD6Pool;
    let use2D6 = this.actor.data.data.use2D6;
    let useAlternity = this.actor.data.data.useAlternity;
    let usesStepMods = true; // for Alternity rolls that use Steps

    let useSineNomine = this.actor.data.data.useSineNomine;
    let rolledInitiative = dataset.name == "Initiative";
    let hideTotal = false;

    let usingedge = this.actor.getFlag("grpga", "usingedge");
    let rushed = this.actor.getFlag("grpga", "rushed");

    switch (dataset.type) {
      case "technique": { // send the notes to the chat and return
        ChatMessage.create(
          {
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            flavor: flavour,
            content: dataset.roll,
          }
        );
        return;
      }
      case "rms": { // no roll, just storing a value
        return;
      }
    }

    // get the modifiers
    let modList = this.fetchRelevantModifiers(this.actor.data, dataset);
    // process the modifiers
    let modformula = "";
    flavour += (dataset.type != "modlist") ? ` [<b>${dataset.roll}</b>]` : `<p class="chatmod">[<b>${dataset.roll}</b>]: ${dataset.name}<br>`;
    let hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (dataset.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    var formula = "";
    switch (dataset.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        isModList = true;
      case "block": // send the value to the chat as a modifiable roll
      case "damage": {// roll damage using a valid dice expression
        formula = dataset.roll;
        usesStepMods = false;
        break;
      }
      case "check": { // a valid dice expression
        formula = dataset.roll;
        isPercent = formula.includes("1d100");
        break;
      }
      case "parry": // a d100 roll less than or equal to a target number
      case "spell": {
        formula = dataset.roll + " - 1d100";
        usesStepMods = false;
        isPercent = true;
        break;
      }
      case "attack": // attacks are subject to critical results
      case "dodge": // a d20 roll to exceed a target number
      case "skill": {
        switch (CONFIG.grpga.hijack) {
          case "UseD6Pool": { // use d6 pools where successes are counted on a 6
            formula = (usingedge) ? `(${dataset.roll + modformula})d6x6cs>4` : `(${dataset.roll + modformula})d6cs>4`;
            modformula = "";
            break;
          }
          case "Use2D10": { // a hijack to roll 2d10 instead
            formula = "2d10 + " + dataset.roll;
            break;
          }
          case "UseD10": { // a hijack to roll d10 instead
            formula = "1d10 + " + dataset.roll;
            break;
          }
          case "Use3D6": { // a hijack to roll 3d6 instead
            formula = "3d6 + " + dataset.roll;
            break;
          }
          case "Use2D6": { // a hijack to roll 2d6 instead
            if (useSineNomine && dataset.type == "attack") { // WWN/SWN attacks
              formula = "1d20 + " + dataset.roll;
            } else if (useSineNomine && dataset.type == "dodge") { // WWN/SWN Save
              formula = "1d20 - " + dataset.roll;
            } else { // normal 2d6 or WWN/SWN skills
              formula = "2d6 + " + dataset.roll;
            }
            break;
          }
          case "UsePercent": { // a hijack to roll percent instead
            formula = dataset.roll + " - 1d100";
            isPercent = true;
            break;
          }
          case "UseAlternity": { // a hijack to roll Control and Situation dice less than a target number
            formula = dataset.roll + " - (1d20";
            let steps = eval(modformula) || 0;
            switch (steps) {
              case -4: formula += " - 1d12)"; break;
              case -3: formula += " - 1d8)"; break;
              case -2: formula += " - 1d6)"; break;
              case -1: formula += " - 1d4)"; break;
              case 0: formula += " + 0d4)"; break;
              case 1: formula += " + 1d4)"; break;
              case 2: formula += " + 1d6)"; break;
              case 3: formula += " + 1d8)"; break;
              case 4: formula += " + 1d12)"; break;
              case 5: formula += " + 1d20)"; break;
              case 6: formula += " + 2d20)"; break;
              case 7: formula += " + 3d20)"; break;
              case 8: formula += " + 4d20)"; break;
              case 9: formula += " + 5d20)"; break;
              case 10: formula += " + 6d20)"; break;
              case 11: formula += " + 7d20)"; break;
              case 12: formula += " + 8d20)"; break;
              case 13: formula += " + 9d20)"; break;
              default: formula += " - 1d20)"; break;
            }
            break;
          }
          default: { // standard Palladium behaviour
            canCrit = true;
            if (useSineNomine && dataset.type == "attack") { // Godbound THAC0
              formula = "20 - 1d20 + " + dataset.roll;
            } else if (useSineNomine && dataset.type == "dodge") { // Godbound Save
              formula = "1d20 - " + dataset.roll;
            } else if (useFAHR && rolledInitiative) { // FAHR initiative roll
              formula = "3d6 + " + dataset.roll;
            } else { // normal d20 attacks
              formula = "1d20 + " + dataset.roll;
            }
          }
        }
        break;
      }
      default: { // there should not be any of these
        console.error("Failed to process this roll type: " + dataset.type);
      }
    }

    // process the modified roll
    let r = await new Roll(formula + ((useAlternity) ? "" : modformula)).evaluate({ async: true });

    if (isModList) { // render the modlist dialog and return
      flavour += `<hr><p>${r.result} = <b>${r.total}</b></p>`;
      new Dialog({
        title: this.actor.name,
        content: flavour,
        buttons: {
          close: {
            icon: "<i class='fas fa-tick'></i>",
            label: "Close"
          },
        },
        default: "close"
      }).render(true)
      return;
    }

    if (useD6Pool) {
      let number = r.dice[0].number;
      let glitch = (rushed) ? 3 : 2;
      let glitches = 0;
      for (let i = 0; i < number; i++) {
        if (r.dice[0].values[i] < glitch) glitches++;
      }
      if (glitches >= number / 2) {
        flavour += ` <p><i class="fas fa-arrow-right"></i> <span class="critfail">Glitched</span></p>`;
      }
    } else if (useSineNomine) { // a roll in Godbound
      hideTotal = true;
      // did a critical success or failure occur?
      let critfail = false;
      let critsuccess = false;
      if (((!use2D6 && dataset.type == "attack") ? r.terms[2].total : r.terms[0].total) <= fail) {
        critfail = true;
      } else if (((!use2D6 && dataset.type == "attack") ? r.terms[2].total : r.terms[0].total) >= threat) {
        critsuccess = true;
      }
      // display the die roll result
      if (use2D6 && dataset.type == "skill") {// WWN/SWN skill roll
        flavour += ` <p>2D6 Roll: [<b>${r.terms[0].total}</b>]`;
      } else if (!use2D6 && dataset.type == "attack") { // godbound THAC0 attack
        flavour += ` <p>D20 Roll: [<b>${r.terms[2].total}</b>]`;
      } else { // all other rolls
        switch (dataset.type) {
          case "attack":
          case "dodge":
          case "skill": {
            flavour += ` <p>D20 Roll: [<b>${r.terms[0].total}</b>]`;
            break;
          }
          default: {
            flavour += ` <p>Result: [<b>${r.total}</b>]`;
          }
        }
      }
      switch (dataset.type) {
        case "dodge": { // all
          if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
          else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
          else if (r.total >= 0) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Success</span>`;
          else flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure</span>`;
          break;
        }
        case "skill": {
          if (use2D6) {
            flavour += ` </p><p>Result: [<b>${r.total}</b>]`
          } else {
            if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
            else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
            else if (r.total >= 21) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Success</span>`;
            else flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure</span>`;
          }
          break;
        }
        case "attack": {
          if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
          else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
          if (use2D6) {
            flavour += ` </p><p>Result: [<b>${r.total}</b>]`
          } else {
            flavour += ` </p><p>Hits AC: [<b>${r.total}</b>]`
          }
        }
      }
      flavour += `</p>`
    } else if (useAlternity && usesStepMods) { // a roll in Alternity RPG
      hideTotal = true;
      const control = r.dice[0].total;
      flavour += ` <p>Control Roll: [<b>${control}</b>]`;
      if (control == 1) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (control == 20) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`

      let result = r.total;
      let sflavour;
      flavour += ` <p>Situation Roll: [<b>${r.dice[1].total}</b>]<hr>Total: [<b>${r.terms[2].total}</b>]`;
      if (result >= 0) {
        // ordinary
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Ordinary Success</span>`;
        let roll = r.terms[2].total;
        if (result >= roll) {
          // good
          sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Good Success</span>`;
          if (result >= roll * 3) {
            //amazing
            sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Amazing Success</span>`;
          }
        }
      } else {
        // failure or marginal
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure/Marginal</span>`;
      }
      flavour += sflavour + `</p>`
    } else if (canCrit) { // an attack or defence roll in D120
      let critfail = false;
      let critsuccess = false;
      if (r.terms[0].total <= fail) {
        critfail = true;
      } else if (r.terms[0].total >= threat) {
        critsuccess = true;
      }
      flavour += ` <p>D20 Roll: [<b>${r.terms[0].total}</b>]`;
      if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`
    } else if (isPercent) { // a skill roll in D120
      let result = r.total;
      let sflavour;
      flavour += ` <p>D100 Roll: [<b>${r.dice[0].values[0]}</b>]`;
      if (result >= 0) {
        // success
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Success</span>`;
        if (useFAHR) {
          let roll = r.dice[0].values[0];
          if (result >= roll) {
            // heroic
            sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Heroic Success</span>`;
            if (result >= roll * 3) {
              //legendary
              sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Legendary Success</span>`;
            }
          }
        }
      } else {
        // failure
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure</span>`;
      }
      flavour += sflavour + `</p>`
    }

    // prepare the flavor in advance based on which type of die is in use.
    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour,
        flags: { hideMessageContent: hideTotal }
      }
    );
    let updates = { ["data.gmod.value"]: 0 };
    if (rolledInitiative) {
      updates["data.bs.value"] = (useD6Pool) ? r.total + Number(dataset.roll) : r.total;
    }
    this.actor.update(updates);
    return;
  }
}
