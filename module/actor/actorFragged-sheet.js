import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorFraggedSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorFragged-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.grpga.testMode) console.debug("entering getData() in ActorFragged-sheet");

    let data = super.getData(options);
    let actor = data.actor;
    let actordata = actor.data;

    data.headerinfo = {};
    let headinfo = data.headerinfo;

    headinfo.level = actordata.dynamic.level;
    headinfo.perception = actordata.dynamic.perception;
    headinfo.initiative = actordata.dynamic.initiative;
    headinfo.dodge = actordata.dynamic.dodge;
    headinfo.xp = actordata.dynamic.xp;
    // for d20
    headinfo.hitpoints = actordata.tracked.hp;
    headinfo.apr = actordata.dynamic.apr;
    headinfo.mpa = actordata.dynamic.mpa;

    // define labels for the header section
    for (let item of data.advantages) {
      switch (item?.name) {
        case "Race": headinfo.race = item; break;
        case "CharacterClass": headinfo.characterclass = item; break;
        case "SpecificInformation": headinfo.specificinfo = item; break;
        case "AdditionalInformation": headinfo.additionalinfo = item; break;
        case "Alignment": headinfo.alignment = item; break;
      }
    }

    // define primary attribute values
    for (let item of data.primaryattributes) {
      if (item.data.moddedvalue < 2) {
        item.data.value = -1;
      } else if (item.data.moddedvalue > 3) {
        item.data.value = 1;
      } else {
        item.data.value = 0;
      }
    }

    // group and sort skills correctly for display
    let tempskills = [...data.skills];
    let tempspells = [...data.spells];
    data.skills = [];
    data.techniques = [];
    for (let item of tempskills) {
      if (item.data.category == "technique") {
        data.techniques.push(item);
      } else {
        data.skills.push(item);
      }
    }
    data.rms = [];
    for (let item of tempspells) {
      if (item.data.category == "rms") {
        data.rms.push(item);
      } else {
        data.skills.push(item);
      }
    }
    for (let item of data.checks) {
      data.skills.push(item);
    }
    delete data.spells;
    delete data.checks;
    data.skills = this.sort(data.skills);

    // group and sort defences correctly for display
    let tempdefences = [...data.defences];
    data.defences = [];
    data.blocks = [];
    for (let item of tempdefences) {
      if (item.data.category == "block") {
        data.blocks.push(item);
      } else {
        data.defences.push(item);
      }
    }

    // group and sort skill mods
    data.checkskillspellmods = this.sort(Array.from(new Set(data.checkmods.concat(data.skillmods, data.spellmods))));

    delete data.skillmods;
    delete data.checkmods;
    delete data.spellmods;

    // check to see if specific modifiers are toggled so we can set the flags
    // the modifier must affect Variable Formulae, D20 skills or D100 skills to work as a toggle (a modifier of zero is ok)
    // these were designed for D6 Pool ruleset but could be used by others
    for (let item of data.checkskillspellmods) {
      switch (item?.name) {
        case "Rushed": this.actor.setFlag("grpga", "rushed", item.data.inEffect); break;
        case "Using Edge": this.actor.setFlag("grpga", "usingedge", item.data.inEffect); break;
      }
    }

    return data;
  }

  sort(data) {
    return data.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importPAL').click(this._onImportPalData.bind(this));
  }

  async _onImportPalData(event) {
    event.preventDefault();
    const scriptdata = this.actor.data.data.itemscript.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.data.data.biography;

    for (let entry of scriptdata) {
      let regex = new RegExp(/([^:]+):([^\r|\n]+)/gi);
      const line = regex.exec(entry);

      switch (line[1].trim()) {
        case "Name": {
          // Name: {{name}}
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
        case "Attribute": {
          // Attribute: {{name}} ### {{abbr || slugify(name)}} ### {{attr || 0}} ### {{sort || 0}} ### {{notes || empty}}
          // Attribute: Intelligence Quotient ### IQ ### 12 ### 1 ### Your ability to reason
          let data = {
            type: "Primary-Attribute",
            data: {
              chartype: "CharacterD120",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1] || this.actor.slugify(result[0]);
          data.data.attr = Number(result[2]) || 0;
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Skill": {
          // Skill: {{name}} ### {{"VarForm" || "D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          // Skill: Athletics ### D100 ### 45 ### 3 ### Training in vigorous exertion for competition
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          if (!result[1]) {
            data.data.category = "spell";
          } else {
            switch (result[1].toLowerCase()) {
              case "varform":
                data.data.category = "check";
                break;
              case "d20":
                data.data.category = "skill";
                break;
              default:
                data.data.category = "spell";
            }
          }
          data.data.formula = result[2] || "0";
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Information": {
          // Information: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "You should be putting html formatted content here to be displayed in the chat log.";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Reference": {
          // Reference: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD120",
              category: "rms"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Defence": {
          // Defence: {{name}} ### {{"D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          if (!result[1]) {
            data.data.category = "parry";
          } else {
            switch (result[1].toLowerCase()) {
              case "d20":
                data.data.category = "dodge";
                break;
              default:
                data.data.category = "parry";
            }
          }
          data.data.formula = result[2] || "0";
          data.data.sort = Number(result[3]) || 50;
          data.data.notes = result[4] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "SharedValue": {
          // SharedValue: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterD120",
              category: "block"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || "0";
          data.data.sort = Number(result[2]) || 50;
          data.data.notes = result[3] || "";
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Armor Type": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name === "Armor Type");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Level": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Level-Ranks"));
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "MeleeDB": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name === "Melee DB");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "MissileDB": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.data.items.find(i => i.name === "Missile DB");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Race": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Race"));
          const update = { _id: item.id, 'name': `Race: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "CharacterClass": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Culture"));
          const update = { _id: item.id, 'name': `Culture: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Occupation": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Profession"));
          const update = { _id: item.id, 'name': `Profession: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Various": {
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterD120",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1];
          switch (result[2]?.toLowerCase()) {
            case "oe": data.data.category = "block"; break;
            case "oeh": data.data.category = "parry"; break;
            default: data.data.category = "dodge"; break;
          }
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Melee": {
          let data = {
            type: "Melee-Attack",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || 0;
          data.data.armourDiv = Number(result[2]) || 0;
          data.data.weight = Number(result[3]) || 0;
          data.data.damage = result[4];
          data.data.damageType = result[5];
          data.data.minST = result[6] || "oeh";
          data.data.reach = Number(result[7]) || 5;
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Ranged": {
          let data = {
            type: "Ranged-Attack",
            data: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || 0;
          data.data.armourDiv = Number(result[2]) || 0;
          data.data.accuracy = Number(result[3]) || 0;
          data.data.range = result[4] || "5";
          data.data.damage = result[5];
          data.data.damageType = result[6];
          data.data.rof = Number(result[7]) || 5;
          data.data.minST = result[8] || "oeh";
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
      }
    }
    await this.actor.update({ 'data.biography': biography });
    await this.actor.update({ 'data.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.fragged.${dataset.type}`, { name: dataset.name });
    let threat = Number(dataset.threat) || 20;
    let fail = 1;
    let canCrit = false, isModList = false, isPercent = false;
    let useFAHR = this.actor.data.data.useFAHR;
    let rolledInitiative = dataset.name == "Initiative";
    let dicetoroll = this.actor.data.data.dynamic["dice-to-roll"].value;

    let usingedge = this.actor.getFlag("grpga", "usingedge");
    let rushed = this.actor.getFlag("grpga", "rushed");

    switch (dataset.type) {
      case "technique": { // send the notes to the chat and return
        ChatMessage.create(
          {
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            flavor: flavour,
            content: dataset.roll,
          }
        );
        return;
      }
      case "rms": { // no roll, just storing a value
        return;
      }
    }

    // get the modifiers
    let modList = this.fetchRelevantModifiers(this.actor.data, dataset);
    // process the modifiers
    let modformula = "";
    flavour += (dataset.type != "modlist") ? ` [<b>${dataset.roll}</b>]` : `<p class="chatmod">[<b>${dataset.roll}</b>]: ${dataset.name}<br>`;
    let hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (dataset.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    var formula = "";
    switch (dataset.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        isModList = true;
      case "damage": // roll damage using a valid dice expression
      case "block": { // send the value to the chat as a modifiable roll
        formula = dataset.roll;
        break;
      }
      case "check": { // a valid dice expression
        formula = dataset.roll;
        isPercent = formula.includes("1d100");
        break;
      }
      case "parry": // a d100 roll less than or equal to a target number
      case "spell": {
        formula = dataset.roll + " - 1d100";
        isPercent = true;
        break;
      }
      case "attack": // attacks are subject to critical results
      case "dodge": // a d20 roll to exceed a target number
      case "skill": {
        formula = `${dicetoroll}d6 + ${dataset.roll}`;
        break;
      }
      default: { // there should not be any of these
        console.error("Failed to process this roll type: " + dataset.type);
      }
    }

    // process the modified roll
    let r = await new Roll(formula + modformula).evaluate({ async: true });

    if (isModList) { // render the modlist dialog and return
      flavour += `<hr><p>${r.result} = <b>${r.total}</b></p>`;
      new Dialog({
        title: this.actor.name,
        content: flavour,
        buttons: {
          close: {
            icon: "<i class='fas fa-tick'></i>",
            label: "Close"
          },
        },
        default: "close"
      }).render(true)
      return;
    }

    if (canCrit) { // an attack or defence roll in D120
      let critfail = false;
      let critsuccess = false;
      if (r.terms[0].total <= fail) {
        critfail = true;
      } else if (r.terms[0].total >= threat) {
        critsuccess = true;
      }

      flavour += ` <p>${dicetoroll}D6 Roll: [<b>${r.terms[0].total}</b>]</p>`;
      if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`
    } else if (isPercent) { // a skill roll in D120
      let result = r.total;
      let sflavour;
      flavour += ` <p>D100 Roll: [<b>${r.dice[0].values[0]}</b>]`;
      if (result >= 0) {
        // success
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Success</span>`;
        if (useFAHR) {
          let roll = r.dice[0].values[0];
          if (result >= roll) {
            // heroic
            sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Heroic Success</span>`;
            if (result >= roll * 3) {
              //legendary
              sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Legendary Success</span>`;
            }
          }
        }
      } else {
        // failure
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure</span>`;
      }
      flavour += sflavour + `</p>`
    } else {
      flavour += ` <p>${dicetoroll}D6 Roll: [<b>${r.terms[0].total}</b>]</p>`;
    }

    // prepare the flavor in advance based on which type of die is in use.
    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour
      }
    );
    let updates = { ["data.gmod.value"]: 0 };
    if (rolledInitiative) {
      updates["data.bs.value"] = (useD6Pool) ? r.total + Number(dataset.roll) : r.total;
    }
    this.actor.update(updates);
    return;
  }
}
