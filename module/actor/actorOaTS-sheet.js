import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorOaTSSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorOaTS-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importOaTS').click(this._onImportOaTSData.bind(this));
  }

  async _onImportOaTSData(event) {
    event.preventDefault();
    ui.notifications.notify("Import for Ops and Tactics System is not yet implemented");
    
    return;
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.oats.${dataset.type}`, { name: dataset.name });
    let threat = Number(dataset.threat) || 18;
    let fail = 3;
    let isDamageOrReaction = false;

    // spells don't get rolled, just displayed
    // this may change in OaTS
    if (dataset.type == "spell") {
      ChatMessage.create(
        {
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: flavour,
          content: dataset.details,
        }
      );
      return;
    }

    var formula = "";
    switch (dataset.type) {
      case "damage": {
        formula = dataset.roll;
        flavour += ` [<b>${dataset.roll}</b>]`;
        isDamageOrReaction = true;
        break;
      }
      case "reaction": { // this is Armour Class
        formula = dataset.roll;
        isDamageOrReaction = true;
        break;
      }
      default: { // this is where die type must be selected and where the threat range must be determined
        formula = "3d6 + " + dataset.roll;
        flavour += ` [<b>${dataset.roll}</b>]`;
        break;
      }
    }

    var modList = this.fetchRelevantModifiers(this.actor.data, dataset);

    var hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += `<p class="chatmod">`;
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        formula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    let r = await new Roll(formula).evaluate({async: true});

    // a Success roll
    if (!isDamageOrReaction) {
      let critfail = false;
      let critsuccess = false;
      if (r.terms[0].total <= fail) {
        critfail = true;
      } else if (r.terms[0].total >= threat) {
        critsuccess = true;
      }

      flavour += ` <p>3D6 Roll: [<b>${r.terms[0].total}</b>]`;
      if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`
    }

    // prepare the flavor in advance based on which type of die is in use.
    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour
      }
    );
    this.actor.update({ ["data.gmod.value"]: 0 });
    return;
  }
}
