export class GRPGATokenDocument extends TokenDocument {

  /**
   * A helper method to retrieve the underlying data behind one of the Token's attribute bars
   * @param {string} barName        The named bar to retrieve the attribute for
   * @param {string} alternative    An alternative attribute path to get instead of the default one
   * @return {object|null}          The attribute displayed on the Token bar, if any
   */
   getBarAttribute(barName, {alternative}={}) {
    const attr = alternative || (barName ? this.data[barName].attribute : null);
    if ( !attr || !this.actor ) return null;
    let data = foundry.utils.getProperty(this.actor.data.data, attr);
    if ( (data === null) || (data === undefined) ) return null;
    const model = game.system.model.Actor[this.actor.type];

    // Single values
    if ( Number.isNumeric(data) ) {
      return {
        type: "value",
        attribute: attr,
        value: Number(data),
        editable: true // super requires a templated attribute to exist
      }
    }

    // Attribute objects
    else if ( ("value" in data) && ("max" in data) ) {
      return {
        type: "bar",
        attribute: attr,
        value: Number(data.value || 0),
        max: Number(data.max || 0),
        min: Number(data.min || 0), // super does not have min values
        editable: true // super requires a templated attribute to exist
      }
    }

    // Otherwise null
    return null;
  }
}

export class GRPGAToken extends Token {
  /**
   * Draw a single resource bar, given provided data
   * @param {number} number       The Bar number
   * @param {PIXI.Graphics} bar   The Bar container
   * @param {Object} data         Resource data for this bar
   * @protected
   */
   _drawBar(number, bar, data) {
    const val = Number(data.value);
    const min = Number(data.min); // super does not have min values
    const pct = Math.clamped(val, 0, data.max) / (data.max); // pct controls the colour
    const pctfull = Math.clamped(val-min, 0, data.max-min) / (data.max-min); // pctfull controls the fill range

    // Determine sizing
    let h = Math.max((canvas.dimensions.size / 12), 8);
    const w = this.w;
    const bs = Math.clamped(h / 8, 1, 2);
    if ( this.data.height >= 2 ) h *= 1.6;  // Enlarge the bar for large tokens

    // Determine the color to use
    const blk = 0x000000;
    let color;
    if ( val <= 0) color = 0x400000; // values less than zero are dark red
    else if ( number === 0 ) color = PIXI.utils.rgb2hex([(1-(pct/2)), pct, 0]);
    else color = PIXI.utils.rgb2hex([(0.5 * pct), (0.7 * pct), 0.5 + (pct / 2)]);

    // Draw the bar
    bar.clear()
    bar.beginFill(blk, 0.5).lineStyle(bs, blk, 1.0).drawRoundedRect(0, 0, this.w, h, 3)
    bar.beginFill(color, 1.0).lineStyle(bs, blk, 1.0).drawRoundedRect(0, 0, pctfull*w, h, 2)

    // Set position
    let posY = number === 0 ? this.h - h : 0;
    bar.position.set(0, posY);
  }
}
