// Import Modules
import { BaseActor } from "./module/actor/baseActor.js";
import { ActorD20Sheet } from "./module/actor/actorD20-sheet.js";
import { ActorFraggedSheet } from "./module/actor/actorFragged-sheet.js";
import { Actor3D6Sheet } from "./module/actor/actor3D6-sheet.js";
import { ActorOaTSSheet } from "./module/actor/actorOaTS-sheet.js";
import { ActorRtWSheet } from "./module/actor/actorRtW-sheet.js";
import { ActorMnMSheet } from "./module/actor/actorMnM-sheet.js";
import { ActorD100Sheet } from "./module/actor/actorD100-sheet.js";
import { ActorD120Sheet } from "./module/actor/actorD120-sheet.js";
import { ActorVsDSheet } from "./module/actor/actorVsD-sheet.js";
import { BaseItem } from "./module/item/item.js";
import { BaseItemSheet, PoolItemSheet, ModifierItemSheet, VariableItemSheet } from "./module/item/item-sheet.js";
import { grpga } from "./module/config.js";
import { GRPGATokenDocument, GRPGAToken } from "./module/token.js";
import { GRPGACombat } from "./module/combat/GRPGACombat.js";
import { VSDCombat, VSDCombatTracker, VSDCombatant, VSDCombatantConfig } from "./module/combat/VSDCombat.js";
import { FengShui2Combat, FengShui2CombatTracker, FengShui2Combatant, FengShui2CombatantConfig } from "./module/combat/FengShui2Combat.js";
import { ActionPointCombat, ActionPointCombatTracker, ActionPointCombatant, ActionPointCombatantConfig } from "./module/combat/actionPointCombat.js";

// Pre-load templates
async function preloadHandlebarsTemplates() {
  const templatePaths = [
    "systems/grpga/templates/partials/vsd-main.hbs",
    "systems/grpga/templates/partials/vsd-skills.hbs",
    "systems/grpga/templates/partials/vsd-spells.hbs",
    "systems/grpga/templates/partials/vsd-header.hbs",
    "systems/grpga/templates/partials/vsd-combat.hbs",
    "systems/grpga/templates/partials/vsd-items.hbs",
    "systems/grpga/templates/partials/d100-header.hbs",
    "systems/grpga/templates/partials/checks.hbs",
    "systems/grpga/templates/partials/defences-saves.hbs",
    "systems/grpga/templates/partials/gmod.hbs",
    "systems/grpga/templates/partials/attacks.hbs",
    "systems/grpga/templates/partials/modifiers.hbs",
    "systems/grpga/templates/partials/variables.hbs",
    "systems/grpga/templates/partials/movement.hbs",
    "systems/grpga/templates/partials/navigation.hbs",
    "systems/grpga/templates/partials/notes.hbs",
    "systems/grpga/templates/partials/owneditems.hbs",
    "systems/grpga/templates/partials/owneditemstab.hbs",
    "systems/grpga/templates/partials/pools.hbs",
    "systems/grpga/templates/partials/primary-attributes.hbs",
    "systems/grpga/templates/partials/reaction-ac.hbs",
    "systems/grpga/templates/partials/scripts.hbs",
    "systems/grpga/templates/partials/sectiontitles.hbs",
    "systems/grpga/templates/partials/skills.hbs",
    "systems/grpga/templates/partials/spells.hbs",
    "systems/grpga/templates/partials/traits.hbs",
    "systems/grpga/templates/partials/mnm-advantages.hbs",
    "systems/grpga/templates/partials/mnm-main.hbs",
    "systems/grpga/templates/partials/mnm-combat.hbs",
    "systems/grpga/templates/partials/mnm-origins.hbs",
    "systems/grpga/templates/partials/mnm-skills.hbs",
    "systems/grpga/templates/partials/mnm-items.hbs",
    "systems/grpga/templates/partials/mnm-owneditems.hbs",
    "systems/grpga/templates/partials/mnm-sectiontitles.hbs",
    "systems/grpga/templates/partials/mnm-scripts.hbs",
    "systems/grpga/templates/partials/mnm-gmod.hbs"
  ];
  return loadTemplates(templatePaths);
};

Roll.CHAT_TEMPLATE = "systems/grpga/templates/dice/roll.hbs";
Roll.TOOLTIP_TEMPLATE = "systems/grpga/templates/dice/tooltip.hbs";

Hooks.once('init', () => {

  CONFIG.grpga = grpga;

  // Define custom Entity classes
  CONFIG.Actor.documentClass = BaseActor;
  CONFIG.Item.documentClass = BaseItem;
  CONFIG.Token.documentClass = GRPGATokenDocument;
  CONFIG.Token.objectClass = GRPGAToken;

  // Which actor in this browser most recently made a roll
  game.settings.register("grpga", "currentActor", {
    name: "Current Actor Name",
    hint: "Which actor most recently made a roll on this browser?",
    scope: "client",
    config: false,
    default: "null",
    type: String
  });

  // Which Ruleset are we using
  game.settings.register("grpga", "rulesetChoice", {
    name: "Ruleset in Play",
    hint: "Which ruleset will you use?",
    scope: "world",
    config: true,
    default: "d120",
    type: String,
    choices: {
      "3d6": "A 3D6 Ruleset",
      "d20": "A D20 Ruleset",
      "d120": "A Palladium Games Ruleset",
      "d100": "A D100 Ruleset (WIP)",
      "vsd": "Against the Darkmaster Ruleset",
      "rtw": "Reclaim the Wild Ruleset",
      "mnm": "Mutants & Masterminds Ruleset",
      "fragged": "Fragged Family Rulesets",
      "oats": "Ops and Tactics System"
    }
  });
  let rschoice = game.settings.get("grpga", "rulesetChoice");
  CONFIG.grpga.ruleset = rschoice;
  CONFIG.statusEffects = CONFIG[`statusEffects${rschoice}`];
  switch (rschoice) {
    case "3d6": {
      CONFIG.grpga.chartype = "Character3D6";
      break;
    }
    case "d100": {
      CONFIG.grpga.chartype = "CharacterD100";
      break;
    }
    case "d120": {
      CONFIG.grpga.chartype = "CharacterD120";
      break;
    }
    case "rtw": {
      CONFIG.grpga.chartype = "CharacterRtW";
      break;
    }
    case "mnm": {
      CONFIG.grpga.chartype = "CharacterMnM";
      break;
    }
    case "fragged": {
      CONFIG.grpga.chartype = "CharacterFragged";
      break;
    }
    case "oats": {
      CONFIG.grpga.chartype = "CharacterOaTS";
      break;
    }
    case "vsd": {
      CONFIG.grpga.chartype = "CharacterVsD";
      break;
    }
    default: {
      CONFIG.grpga.chartype = "CharacterD20";
      break;
    }
  }

  if (rschoice == "d120") {
    // Are we hijacking Palladium for another game system
    game.settings.register("grpga", "palladiumHijack", {
      name: "SETTINGS.HijackPalladium",
      hint: "SETTINGS.HijackPalladiumHint",
      scope: "world",
      config: true,
      default: "UsePal",
      type: String,
      choices: {
        "UsePal": "SETTINGS.UsePal",
        "UseD6Pool": "SETTINGS.UseD6Pool",
        "UseD20": "SETTINGS.UseD20",
        "UseD10": "SETTINGS.UseD10",
        "Use2D10": "SETTINGS.Use2D10",
        "Use3D6": "SETTINGS.Use3D6",
        "Use2D6": "SETTINGS.Use2D6",
        "UseAlternity": "SETTINGS.UseAlternity",
        "UsePercent": "SETTINGS.UsePercent"
      }
    });
    CONFIG.grpga.hijack = game.settings.get("grpga", "palladiumHijack");

    // Register whether modifying a hijack with Sine Nomine conventions
    game.settings.register("grpga", "useSineNomine", {
      name: "SETTINGS.UseSineNomine",
      hint: "SETTINGS.UseSineNomineHint",
      scope: "world",
      config: true,
      default: false,
      type: Boolean
    });
    CONFIG.grpga.useSineNomine = game.settings.get("grpga", "useSineNomine");
  } else {
    CONFIG.grpga.hijack = "UsePal";
    CONFIG.grpga.useSineNomine = false;
  }

  // Which Rank counting method are we using?
  game.settings.register("grpga", "rankMode", {
    name: "Ranks counting mode",
    hint: "How do you want to count ranks?",
    scope: "world",
    config: true,
    default: "111",
    type: String,
    choices: {
      "111": "All ranks count for one point",
      "521": "0, 10 fives, 10 twos, then ones",
      "1521": "-15, 10 fives, 10 twos, then ones",
      "0521": "-25, 10 fives, 10 twos, then ones",
      "0321h": "-15, 10 threes, 10 twos, 10 ones, then halves",
      "05321": "-25, 10 fives, 10 threes, ten twos, then ones",
      "0521h": "-25, 10 fives, ten twos, 10 ones, then halves"
    }
  });

  // Register Combat Tracker in use
  game.settings.register("grpga", "combatTrackerInUse", {
    name: "SETTINGS.CombatTrackerInUse",
    hint: "SETTINGS.CombatTrackerInUseHint",
    scope: "world",
    config: true,
    default: "default",
    type: String,
    choices: {
      "default": "Foundry Default",
      "vsd": "Against the Darkmaster",
      "pal": "Palladium/ShadowRun4",
      "fs2": "Feng Shui 2 (Testing)"
    }
  });
  CONFIG.Combat.tracker = game.settings.get("grpga", "combatTrackerInUse");

  game.settings.register("grpga", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: true,
    type: Number,
    default: 1.41
  });

  game.settings.register("grpga", "systemRefreshOnReady", {
    name: "Refresh items and actors on Ready",
    scope: "world",
    config: false,
    type: Boolean,
    default: false
  });

  // Register initiative rule
  game.settings.register("grpga", "initiativeRule", {
    name: "SETTINGS.InitRule",
    hint: "SETTINGS.InitHint",
    scope: "world",
    config: true,
    default: "vsd",
    type: String,
    choices: {
      "default": "SETTINGS.InitDefault",
      "house": "SETTINGS.InitHouse",
      "house2": "SETTINGS.InitHouse2",
      "dnd": "SETTINGS.D20",
      "d100": "SETTINGS.D100",
      "vsd": "SETTINGS.VsD",
      "oats": "SETTINGS.OaTS",
      "percent": "SETTINGS.percent"
    },
    onChange: rule => _setBase4eInitiative(rule)
  });
  _setBase4eInitiative(game.settings.get("grpga", "initiativeRule"));

  function _setBase4eInitiative(initMethod) {
    let formula;
    switch (initMethod) {
      case "default": formula = "(1d100 - 1) / 1000000 + @dynamic.dx.value / 10000 + @bs.value"; break;
      case "house": formula = "2d100 / 1000 + @dynamic.dx.value / 1000 + @bs.value"; break;
      case "house2": formula = "(1d750 / 1000) -.251 + @bs.value"; break;
      case "dnd": formula = "1d20 + @bs.value + (1d100 - 1) / 100"; break;
      case "oats": formula = "3d6 + @bs.value + (1d100 - 1) / 100"; break;
      case "d100": formula = "2d10 + @bs.value"; break;
      case "vsd": formula = "@bs.value"; break;
      case "percent": formula = "1d10 + @dynamic.initiative.value"; break;
    }

    let decimals = (initMethod == "default") ? 6 : 3;
    CONFIG.Combat.initiative = {
      formula: formula,
      decimals: decimals
    }
  }

  if (CONFIG.grpga[CONFIG.grpga.ruleset]) {
    // Register Defence Variables
    game.settings.register("grpga", "defenceVariables", {
      name: "SETTINGS.DefenceVariables",
      hint: "SETTINGS.DefenceVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.grpga[CONFIG.grpga.ruleset].defencevariables,
      type: String
    });
    CONFIG.grpga[CONFIG.grpga.ruleset].defencevariables = game.settings.get("grpga", "defenceVariables").split(",").map(word => word.trim());

    // Register Attack Variables
    game.settings.register("grpga", "attackVariables", {
      name: "SETTINGS.AttackVariables",
      hint: "SETTINGS.AttackVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.grpga[CONFIG.grpga.ruleset].attackvariables,
      type: String
    });
    CONFIG.grpga[CONFIG.grpga.ruleset].attackvariables = game.settings.get("grpga", "attackVariables").split(",").map(word => word.trim());

    // Register Skill Variables
    game.settings.register("grpga", "skillVariables", {
      name: "SETTINGS.SkillVariables",
      hint: "SETTINGS.SkillVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.grpga[CONFIG.grpga.ruleset].skillvariables,
      type: String
    });
    CONFIG.grpga[CONFIG.grpga.ruleset].skillvariables = game.settings.get("grpga", "skillVariables").split(",").map(word => word.trim());

    // Register Spell Variables
    game.settings.register("grpga", "spellVariables", {
      name: "SETTINGS.SpellVariables",
      hint: "SETTINGS.SpellVariablesHint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.grpga[CONFIG.grpga.ruleset].spellvariables,
      type: String
    });
    CONFIG.grpga[CONFIG.grpga.ruleset].spellvariables = game.settings.get("grpga", "spellVariables").split(",").map(word => word.trim());
  };

  // Register Automatically making critical rolls
  game.settings.register("grpga", "autoCriticalRolls", {
    name: "SETTINGS.AutoCriticalRolls",
    hint: "SETTINGS.AutoCriticalRollsHint",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  // Register Show Test Data
  game.settings.register("grpga", "showTestData", {
    name: "SETTINGS.ShowTestData",
    hint: "SETTINGS.ShowTestDataHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.grpga.testMode = game.settings.get("grpga", "showTestData");

  // Register Show Hooks Data
  game.settings.register("grpga", "showHooks", {
    name: "SETTINGS.ShowHooks",
    hint: "SETTINGS.ShowHooksHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.debug.hooks = game.settings.get("grpga", "showHooks");

  // Register Resetting of Temporary Items on sheet closing
  game.settings.register("grpga", "resetTemps", {
    name: "SETTINGS.ResetTemps",
    hint: "SETTINGS.ResetTempsHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.grpga.resetTemporaryItemsOnSheetClose = game.settings.get("grpga", "resetTemps");

  // Register whether using FourAmigos House Rules
  game.settings.register("grpga", "useFAHR", {
    name: "SETTINGS.UseFAHR",
    hint: "SETTINGS.UseFAHRHint",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });
  // ================ end of system settings

  // ================ beginning of system setup
  if (CONFIG.Combat.tracker == "pal") {
    CONFIG.Combat.documentClass = ActionPointCombat;
    CONFIG.ui.combat = ActionPointCombatTracker;
    CONFIG.Combatant.documentClass = ActionPointCombatant;
    CONFIG.Combat.sheetClass = ActionPointCombatantConfig;
    CONFIG.time.roundTime = 10;
  } else if (CONFIG.Combat.tracker == "fs2") {
    CONFIG.Combat.documentClass = FengShui2Combat;
    CONFIG.ui.combat = FengShui2CombatTracker;
    CONFIG.Combatant.documentClass = FengShui2Combatant;
    CONFIG.Combat.sheetClass = FengShui2CombatantConfig;
    CONFIG.time.roundTime = 80;
  } else if (CONFIG.Combat.tracker == "vsd") {
    CONFIG.Combat.documentClass = VSDCombat;
    CONFIG.ui.combat = VSDCombatTracker;
    CONFIG.Combatant.documentClass = VSDCombatant;
    CONFIG.Combat.sheetClass = VSDCombatantConfig;
    CONFIG.time.roundTime = 10;
  } else {
    // use the Foundry Default settings
    CONFIG.Combat.documentClass = GRPGACombat;
  }

  // Change the thickness of the border around Objects. Default = 4
  CONFIG.Canvas.objectBorderThickness = 15;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("grpga", ActorD20Sheet, {
    types: ["CharacterD20"],
    makeDefault: true,
    label: "D20 Character"
  });
  Actors.registerSheet("grpga", ActorD120Sheet, {
    types: ["CharacterD120"],
    makeDefault: true,
    label: "Palladium Character"
  });
  Actors.registerSheet("grpga", ActorRtWSheet, {
    types: ["CharacterRtW"],
    makeDefault: true,
    label: "Reclaim the Wild Character"
  });
  Actors.registerSheet("grpga", ActorMnMSheet, {
    types: ["CharacterMnM"],
    makeDefault: true,
    label: "Mutants & Masterminds Character"
  });
  Actors.registerSheet("grpga", ActorFraggedSheet, {
    types: ["CharacterFragged"],
    makeDefault: true,
    label: "Fragged Family Character"
  });
  Actors.registerSheet("grpga", Actor3D6Sheet, {
    types: ["Character3D6"],
    makeDefault: true,
    label: "Dynamic 3D6 Character"
  });
  Actors.registerSheet("grpga", ActorOaTSSheet, {
    types: ["CharacterOaTS"],
    makeDefault: true,
    label: "Dynamic OaTS Character"
  });
  Actors.registerSheet("grpga", ActorD100Sheet, {
    types: ["CharacterD100"],
    makeDefault: true,
    label: "Dynamic D100 Character"
  });
  Actors.registerSheet("grpga", ActorVsDSheet, {
    types: ["CharacterVsD"],
    makeDefault: true,
    label: "Dynamic VsD Character"
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("grpga", BaseItemSheet, {
    types: ["Primary-Attribute", "Melee-Attack", "Ranged-Attack", "Container", "Rollable", "Power", "Trait", "Defence", "Equipment", "Hit-Location", "Points", "Ingredient", "Advantage"],
    makeDefault: true,
    label: "Items"
  });
  Items.registerSheet("grpga", PoolItemSheet, {
    types: ["Pool"],
    makeDefault: true,
    label: "Pool Item"
  });
  Items.registerSheet("grpga", ModifierItemSheet, {
    types: ["Modifier"],
    makeDefault: true,
    label: "Modifier Item"
  });
  Items.registerSheet("grpga", VariableItemSheet, {
    types: ["Variable"],
    makeDefault: true,
    label: "Variable Item"
  });

  preloadHandlebarsTemplates();

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function () {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('debug', function (text, content) {
    return console.debug(text, content);
  });

  Handlebars.registerHelper('toSentenceCase', function (str) {
    return str.replace(
      /\w\S*/g,
      function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  });

  Handlebars.registerHelper('times', function (n, content) {
    let result = "";
    for (let i = 0; i < n; ++i) {
      result += content.fn(i);
    }
    return result;
  });

  /**
   * A helper to create a set of radio checkbox input elements in a named set.
   * The provided keys are the possible radio values while the provided values are human readable labels.
   *
   * @param {string} name         The radio checkbox field name
   * @param {object} choices      A mapping of radio checkbox values to human readable labels
   * @param {string} options.checked    Which key is currently checked?
   * @param {boolean} options.localize  Pass each label through string localization?
   * @return {Handlebars.SafeString}
   *
   * @example <caption>The provided input data</caption>
   * let groupName = "importantChoice";
   * let choices = {a: "Choice A", b: "Choice B"};
   * let chosen = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <div class="form-group">
   *   <label>Radio Group Label</label>
   *   <div class="form-fields">
   *     {{radioBoxes groupName choices checked=chosen localize=true}}
   *   </div>
   * </div>
   */
  Handlebars.registerHelper('radioLabels', function (name, choices, options) {
    const checked = options.hash['checked'] || null;
    const localize = options.hash['localize'] || false;
    let html = "";
    for (let [key, label] of Object.entries(choices)) {
      if (localize) label = game.i18n.localize(label);
      const isChecked = checked === key;
      html += `<input type="radio" id="${label}" name="${name}" value="${key}" ${isChecked ? "checked" : ""}><label for="${label}">${label}</label>`;
    }
    return new Handlebars.SafeString(html);
  });
});

/* I think I will have to include lib-targeting code in order to rely on this

const mod = "lib-targeting";

export class SampleClass {

    targetsTable = {};

    static async ready() {
        NPCTargeting = window.NPCTargeting;

        try {
            SampleClass.targetsTable = new TargetsTable(mod);
        } catch(error) {
            ui.notifications.error("You need to load the Lib-Targeting Module");
        }

        NPCTargeting.init(SampleClass.targetsTable);
    } // -- end ready


    static async targetTokenHandler(user,token,targeted) {
        await NPCTargeting.targetTokenHandler(user,token,targeted);

        console.debug(mod,"Token is Targeted By:\n", SampleClass.targetsTable.getTargetSources(token));
        console.debug(mod,"User is targeting:\n", SampleClass.targetsTable.getSourceTargets(user));
    }

    static async controlTokenHandler(token, tf) {
        await NPCTargeting.controlTokenHandler(token, tf);
    }
}

Hooks.on("ready",SampleClass.ready);
Hooks.on("targetToken", SampleClass.targetTokenHandler);
Hooks.on("controlToken",SampleClass.controlTokenHandler);
*/
