# grpga

Foundry Virtual TableTop support for a Generic RolePlaying Game Aid (GRPGA).

The primary goal of GRPGA is to permit users to play their game, in their way, online at least as easily as if they were sitting around a table; any further automation, character generation or advancement tracking features have been added for convenience where they make sense.

GRPGA currently supports play of:
- GURPS (3D6)
- Against the Darkmaster (VsD) - with combat tracker
- Palladium games (D120) - with combat tracker
- Hijacks of the Palladium ruleset swap the Skills, Defences and Attacks D20 rolls for other systems dice:
    - D20 (changes the header and import for now)
    - 2D10
    - 3D6 (to exceed a target number)
    - 2D6
    - Percentile
- Rolemaster family of games (D100)
- D20 games - I will be retiring this ruleset soon as it can be run under D120 with a hijack. A conversion to D120 button is on the scripts tab of each actor.
- Ops and Tactics System (OaTS) - I will likely retire this game system soon as well because it too can run under D120 with a hijack.
- Fragged - (Very much WIP) - An exploration into the feasability of running the upcoming Fragged Empires2 game system.

Development of a specific ruleset depends on the active participation and feedback of players and GMs. If you want to see something for your ruleset, then ask for it and show me how it needs to work.

A long-term goal is to capture the advantages of each system and create my own based on timeline tactical activity instead of turn-based action.

To join the discussion or monitor progress on Discord, join here; https://discord.gg/d8ujbNG Please let me know on arrival which game system or homebrew effort led you to investigate my project. Feel free to ask for guidance or assistance. Thank you for your interest.

If you would like to support me, you may do so here; https://www.patreon.com/jbhuddleston or here; https://ko-fi.com/jbhuddleston Thank you.
