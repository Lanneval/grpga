// Open a dialog for quickly changing token vision parameters of the controlled tokens.
// This macro was written by @Sky#9453
// https://github.com/Sky-Captain-13/foundry
// Adapted for GRPGA by @JamesH (UTC-3)#9963
// https://gitlab.com/jbhuddleston/grpga

if (canvas.tokens.controlled.length === 0)
    return ui.notifications.error("Please select a token first");

let applyChanges = false;
new Dialog({
    title: `Token Vision Configuration`,
    content: `
<form>
    <div class="form-group">
        <label>Light Conditions:</label>
        <select id="light-conditions" name="light-conditions">
            <option value="nochange">No Change</option>
            <option value="dark">Darkness</option>
            <option value="star">Starlight</option>
            <option value="moon">Moonlight</option>
            <option value="dim3">Dim Light (3 m)</option>
            <option value="dim6">Dim Light (6 m)</option>
            <option value="dim9">Dim Light (9 m)</option>
            <option value="dim12">Dim Light (12 m)</option>
            <option value="bright6">Bright Light (6 m)</option>
            <option value="bright12">Bright Light (12 m)</option>
            <option value="bright18">Bright Light (18 m)</option>
            <option value="bright24">Bright Light (24 m)</option>
        </select>
    </div>
    <div class="form-group">
        <label>Light Source:</label>
        <select id="light-source" name="light-source">
            <option value="nochange">No Change</option>
            <option value="none">None</option>
            <option value="hooded-dim">Lantern (Hooded - Dim)</option>
            <option value="candle">Candle</option>
            <option value="light">Light (Cantrip)</option>
            <option value="torch">Torch</option>
            <option value="lamp">Lamp</option>
            <option value="hooded-bright">Lantern (Hooded - Bright)</option>
            <option value="bullseye">Lantern (Bullseye)</option>
        </select>
    </div>
</form>
    `,
    buttons: {
        yes: {
            icon: "<i class='fas fa-check'></i>",
            label: `Apply Changes`,
            callback: () => applyChanges = true
        },
        no: {
            icon: "<i class='fas fa-times'></i>",
            label: `Cancel Changes`
        },
    },
    default: "yes",
    close: html => {
        if (applyChanges) {
            for (let token of canvas.tokens.controlled) {
                let visionType = token.actor?.data.data.dynamic.visionType || "normal";
                let lightConditions = html.find('[name="light-conditions"]')[0].value || "none";
                let lightSource = html.find('[name="light-source"]')[0].value || "none";
                let dimSight = 0;
                let brightSight = 0;
                let dimLight = 0;
                let brightLight = 0;
                let lightAngle = 360;
                let lockRotation = token.data.lockRotation;
                // Get Vision Type Values
                switch (visionType) {
                    case "normal":
                        switch (lightConditions) {
                            case "dark": {
                                dimSight = 0;
                                brightSight = 0;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "dim3": {
                                dimSight = 3;
                                brightSight = 0;
                                break;
                            }
                            case "dim6": {
                                dimSight = 6;
                                brightSight = 0;
                                break;
                            }
                            case "dim9": {
                                dimSight = 9;
                                brightSight = 0;
                                break;
                            }
                            case "dim12": {
                                dimSight = 12;
                                brightSight = 0;
                                break;
                            }
                            case "bright6": {
                                dimSight = 12;
                                brightSight = 6;
                                break;
                            }
                            case "bright12": {
                                dimSight = 24;
                                brightSight = 12;
                                break;
                            }
                            case "bright18": {
                                dimSight = 36;
                                brightSight = 18;
                                break;
                            }
                            case "bright24": {
                                dimSight = 48;
                                brightSight = 24;
                                break;
                            }
                            default: {
                                dimSight = token.data.dimSight;
                                brightSight = token.data.brightSight;
                            }
                        }
                        break;
                    case "Star Sight":
                    case "Keen Senses":
                        switch (lightConditions) {
                            case "dark": {
                                dimSight = 0;
                                brightSight = 0;
                                break;
                            }
                            case "dim3":
                            case "dim6":
                            case "dim9":
                            case "dim12": {
                                dimSight = 30;
                                brightSight = 15;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "bright6":
                            case "bright12":
                            case "bright18":
                            case "bright24": {
                                dimSight = 240;
                                brightSight = 120;
                                break;
                            }
                            default: {
                                dimSight = token.data.dimSight;
                                brightSight = token.data.brightSight;
                            }
                        }
                        break;
                    case "Dark Sight":
                        switch (lightConditions) {
                            case "dark": {
                                dimSight = 3;
                                brightSight = 0;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "dim3":
                            case "dim6":
                            case "dim9":
                            case "dim12": {
                                dimSight = 30;
                                brightSight = 15;
                                break;
                            }
                            case "bright6": {
                                dimSight = 24;
                                brightSight = 12;
                                break;
                            }
                            case "bright12": {
                                dimSight = 48;
                                brightSight = 24;
                                break;
                            }
                            case "bright18": {
                                dimSight = 72;
                                brightSight = 36;
                                break;
                            }
                            case "bright24": {
                                dimSight = 96;
                                brightSight = 48;
                                break;
                            }
                            default: {
                                dimSight = token.data.dimSight;
                                brightSight = token.data.brightSight;
                            }
                        }
                        break;
                    case "Night Sight":
                        switch (lightConditions) {
                            case "dark": {
                                dimSight = 3;
                                brightSight = 0;
                                break;
                            }
                            case "dim3":
                            case "dim6":
                            case "dim9":
                            case "dim12": {
                                dimSight = 30;
                                brightSight = 15;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "bright6":
                            case "bright12":
                            case "bright18":
                            case "bright24": {
                                dimSight = 240;
                                brightSight = 120;
                                break;
                            }
                            default: {
                                dimSight = token.data.dimSight;
                                brightSight = token.data.brightSight;
                            }
                        }
                        break;
                    case "Darkvision":
                        dimSight = 240;
                        brightSight = 120;
                        break;
                }
                // Get Light Source Values
                switch (lightSource) {
                    case "none":
                        dimLight = 0;
                        brightLight = 0;
                        break;
                    case "hooded-dim":
                        dimLight = 1.5;
                        brightLight = 0;
                        break;
                    case "candle":
                        dimLight = 3;
                        brightLight = 1.5;
                        break;
                    case "light":
                        dimLight = 6;
                        brightLight = 3;
                        break;
                    case "torch":
                        dimLight = 12;
                        brightLight = 6;
                        break;
                    case "lamp":
                        dimLight = 13.5;
                        brightLight = 4.5;
                        break;
                    case "hooded-bright":
                        dimLight = 18;
                        brightLight = 9;
                        break;
                    case "bullseye":
                        dimLight = 36;
                        brightLight = 18;
                        lockRotation = false;
                        lightAngle = 52.5;
                        break;
                    case "nochange":
                    default:
                        dimLight = token.data.dimLight;
                        brightLight = token.data.brightLight;
                        lightAngle = token.data.lightAngle;
                        lockRotation = token.data.lockRotation;
                }
                let updateData = {
                    vision: true,
                    visionType: visionType,
                    dimSight: dimSight,
                    brightSight: brightSight,
                    dimLight: dimLight,
                    brightLight: brightLight,
                    lightAlpha: 0.3,
                    lightColor: "#808040",
                    lightAngle: lightAngle,
                    lockRotation: lockRotation
                };
                // Update Token
                console.debug(`Updating Light settings for ${token.data.name} with ${visionType} vision:\n`, updateData);
                token.update(updateData);
            }
        }
    }
}).render(true);